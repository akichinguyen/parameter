

                                 INSTALLATION

                             M O D E L L E R   9.24


                       Copyright(c) 1989-2020 Andrej Sali
                              All Rights Reserved

 See the file README for information about how to get MODELLER. The source code
 is not generally available. Hence, most users are limited to the compiled
 versions of MODELLER. The program is distributed as a single install file
 that contains scripts, libraries, examples, documentation (in PDF and
 HTML formats) and executables for the supported platforms and operating
 systems. Please refer to the relevant section below for your platform:


** WINDOWS INSTALLATION

   1)   Log on as a Computer Administrator.

   2)   Download the Windows installer modeller9.24-32bit.exe
        or modeller9.24-64bit.exe and save it to your Desktop.

   3)   Double-click on the modeller9.24-32bit or modeller9.24-64bit
        file to start the installer.

   4)   Tell the installer where to install Modeller, and enter your Modeller
        license key when prompted.

   5)   Once the install is complete, you can run Modeller Python scripts
        like any other Python script - download Python from www.python.org
        (any version between 2.3 and 3.8) and open the scripts in Python's
        IDLE environment. Alternatively, if you do not want to or cannot
        install Python, use the Modeller link from the Start Menu to start
        a Command Prompt from where you can run Modeller scripts.

   6)   You can then delete the original installer file from your Desktop.


** MAC OS X INSTALLATION

   1)   Log in to your Mac as an admin user.

   2)   Download the modeller-9.24.dmg file to your Desktop.

   4)   Double-click on the modeller-9.24.dmg file to open the disk image.

   5)   Double-click on the "Modeller 9.24.pkg" file within this image.
        Enter your license key when prompted by the installer.

   6)   Once the install is complete, you can run Modeller scripts from
        a command line (e.g. the Terminal application) by typing
        "python foo.py". Alternatively, you can type "mod9.24" if Python
        on your machine does not work correctly for some reason.

   7)   You can then drag both the 'Modeller 9.24' disk image and the
        modeller-9.24.dmg file to your trash.


** LINUX INSTALLATION (USING RPM)

   (Note: replace 'i386' with 'x86_64' if using a 64-bit AMD64/EM64T machine.)

   1)  Download the modeller-9.24-1.i386.rpm file.

   2)  Install the RPM file with the following command, replacing XXXX with
       your Modeller license key:

          env KEY_MODELLER=XXXX rpm -Uvh modeller-9.24-1.i386.rpm

   3)  You can then run Modeller scripts by typing "python foo.py".


** GENERIC UNIX INSTALLATION

   1)  Download the modeller-9.24.tar.gz file into a temporary directory on your
       computer.

   2)  Open a console or terminal (e.g. xterm, Konsole, GNOME terminal)
       and change to the directory where you downloaded the .tar.gz file.
       Unpack the file with the following commands:

          gunzip modeller-9.24.tar.gz
          tar -xvf modeller-9.24.tar

      The result of unpacking will be the directory ./modeller-9.24, containing
      the following uncompressed files and directories:

         doc/             MODELLER documentation directory
         examples/        directory containing examples and tutorials
         Install          installation script
         INSTALLATION     this file
         README           file describing distribution and registration
         modlib/          libraries and data files for the program
         bin/             .top script files and MODELLER executables


   3)  Go to the ./modeller-9.24 directory and run the installation script:

         ./Install

       Answer several questions as prompted. If you make a mistake,
       you can re-run the script.

  For additional information visit our web site:

  https://salilab.org/modeller/



  Sincerely,

  MODELLER Team
  2020/04/06, r11614
