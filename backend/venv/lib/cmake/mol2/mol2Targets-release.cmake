#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "mol2" for configuration "Release"
set_property(TARGET mol2 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(mol2 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libmol2.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS mol2 )
list(APPEND _IMPORT_CHECK_FILES_FOR_mol2 "${_IMPORT_PREFIX}/lib/libmol2.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
