#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "sampling" for configuration "Release"
set_property(TARGET sampling APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(sampling PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libsampling.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS sampling )
list(APPEND _IMPORT_CHECK_FILES_FOR_sampling "${_IMPORT_PREFIX}/lib/libsampling.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
