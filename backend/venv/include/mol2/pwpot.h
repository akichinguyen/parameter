#ifndef _MOL_PWPOT_H_
#define _MOL_PWPOT_H_

#include "atom_group.h"
#include "prms.h"
#include "nbenergy.h"

/**
 * Calculate knowledge-based pairwise potential.
 *
 * Parameters are taken from "pwpot" section of libmol parameters file. Atoms with unknown types are ignored.
 *
 * This function is primarily intended to be used with DARS (Decoys As the Reference State), but can be used with any
 * pairwise atom-level potential, provided it is decomposed by eigenvalues.
 *
 * Only single-bin potentials are supported; the error will be issued if called with multiple pwpot in \p atomprm.
 * This is because it's very non-trivial to properly make transitions between different bins, and so far it was not needed.
 *
 * DARS is a knowledge-based pairwise potential.
 * While it can be used on its own, for docking it's suggested to use it in addition to a common
 * forcefield (VdW, electrostatics, etc) [1].
 *
 * The DARS potential was introduced in [1], with energy values given in Table S1.
 * However, in practice we use tables given in [2], Table 1.
 * However, it is usually better [2] to use weighted sum of DARS and ACS [3] energies.
 * This choice does not affect this function -- it uses table of pairwise energies from \p atomprm.
 *
 * The energy value is atom-type-dependent constant for atoms within cutoff (pwpot.r2 - delta).
 * Then it slowly switches to zero during 2*delta range after the cutoff.
 * By default, delta = 0.5 A.
 *
 * Switching to zero works as follows:
 *  E_ij = E_0 * (1 - 6*p^5 + 15*p^4 - 10*p^3), where p = (r_ij - (pwpot.r2 - delta)) / (2 * delta).
 * This polynomial was chosen because it offers smooth and monotonous transition from E_0 to 0 as r_ij changes from pwpot.r2 - delta to pwpot.r2 + delta.
 *
 * The force in range [0; pwpot.r2 - delta] and [pwpot.r2 + delta; +inf) are equal to zero since energy is constant.
 * In the intermediate range [pwpot.r2 - delta; pwpot.r2 + delta], the derivative of energy is:
 *  dE_ij / dr_ij = - E_0 * 30 * (p-1)^2 * p^2 / (2 * delta)
 *
 * References:
 * [1] Chuang et al. DARS (Decoys As the Reference State) potentials for protein-protein docking.
 *     Biophys. J. 95, 4217–4227 (2008). https://doi.org/10.1529/biophysj.108.135814
 * [2] Kozakov et al. PIPER: An FFT-based protein docking program with pairwise potentials.
 *     Proteins Struct. Funct. Bioinforma. 65, 392–406 (2006). https://doi.org/10.1002/prot.21117
 * [3] Zhang et al. Determination of atomic desolvation energies from the structures of crystallized proteins.
 *     J. Mol. Biol. 267, 707–726 (1997). https://doi.org/10.1006/jmbi.1996.0859
 *
 * \param ag Atom group; atom gradients are changed.
 * \param energy The pointer where resulting energy is added. Just a single \c double, not an array.
 * \param atomprm Atom parameters (from \ref mol_prms_read).
 * \param nblst Non-bonded list.
 * \param weight Energy scale.
 */
void mol_pwpot_eng(
	struct mol_atom_group *ag,
	double *energy,
	const struct mol_prms *atomprm,
	const struct nblist *nblst,
	const double weight);

/**
 * Calculate knowledge-based pairwise potential.
 *
 * The potential is the same as in \ref mol_pwpot_eng, with the following differences:
 * - Multiple-bin potentials can be used. To avoid double summation, right edge of the bin is not inclusive.
 * - No switching to zero. Abrupt shifts at bin edges.
 * - No gradient computation.
 * - No non-bonded lists, interaction between two atom_groups are scored. If both groups are the same, self-interactions will be included.
 *
 * \param ag1 First atom group.
 * \param ag2 Second atom group.
 * \param atomprm Atom parameters (from \ref mol_prms_read).
 * \return Total interaction energy.
 */
double mol_pwpot_score(
	const struct mol_atom_group *ag1,
	const struct mol_atom_group *ag2,
	const struct mol_prms *atomprm);

#endif /** _MOL_PWPOT_H_ **/
