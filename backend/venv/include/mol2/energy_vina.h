#ifndef _MOL_ENERGY_VINA_H_
#define _MOL_ENERGY_VINA_H_

#include "atom_group.h"
#include "geometry.h"
#include "lists.h"


// The forcefield is described in [1]. Equation numbers are given according to this paper.
// [1]. Trott, O. & Olson, A. J. AutoDock Vina: Improving the speed and accuracy of docking with a new scoring
// function, efficient optimization, and multithreading. J. Comput. Chem. 8, 455-461 (2009).

///! List of components of energy function. Can be used to specify weights or return per-component energies.
struct mol_vina_components {
	double gauss1, gauss2, repuls, hydrph, hydrbn, branch;
};

///! List of Vina atom types.
enum mol_vina_atom_type {
	XS_ATOM_C = 0, ///< C attached to only other Cs and Hs
	XS_ATOM_C_HET = 1, ///< C attached to heteroatom (not C or H)
	XS_ATOM_N = 2, ///< N that's neither donor nor acceptor
	XS_ATOM_N_D = 3, ///< N donor
	XS_ATOM_N_A = 4, ///< N acceptor
	XS_ATOM_N_AD = 5, ///< N that's both donor and acceptor
	XS_ATOM_O = 6,
	XS_ATOM_O_D = 7,
	XS_ATOM_O_A = 8,
	XS_ATOM_O_AD = 9,
	XS_ATOM_S = 10,
	XS_ATOM_P = 11,
	XS_ATOM_F = 12,
	XS_ATOM_Cl = 13,
	XS_ATOM_Br = 14,
	XS_ATOM_I = 15,
	XS_ATOM_METAL = 16,
	XS_ATOM_X = 17  ///< Any non-interacting atom, including H
};

///! Parameters of Vina potential function. Not supposed to be edited manually, but you can try.
struct mol_vina_params {
	struct mol_vina_components weights; ///< Weight of different components of Vina potential.
	size_t lig_index_first, lig_index_last; ///< First and last (inclusive) indices of ligand atoms.
	double cutoff_sq; ///< Squared cut-off.
	bool *exclmat; ///< Matrix of 0-1, 0-2, 0-3 bonds in ligand.
	enum mol_vina_atom_type *atom_types; ///< Vina atom types for all atoms in the atomgroup.
	struct mol_index_list rec_list; ///< List of receptor atoms in the box.
	struct mol_vector3 box_min, box_max;
};

/**
 * Calculate internal energy of the ligand.
 *
 * The returned energy differs from the intramolecular term of AutoDock Vina!
 * AutoDock Vina excludes pairs that don't move relative to each other, and we don't.
 * The list of covalently bound atoms might also differ due to different algorithms for inferring them.
 * Other than that, the potentials are the same.
 *
 * \param ag Atomgroup. Gradients are updated.
 * \param energy Energy value to return.
 * \param energies_detailed Per-component energy values to return, can be NULL. \c branch is set to 0.0.
 * \param vp Parameters of Vina potential. See \ref mol_vina_params_init.
 */
void mol_vina_energy_intra(
	struct mol_atom_group *ag,
	double *energy,
	struct mol_vina_components *energies_detailed,
	const struct mol_vina_params *vp);

/**
 * Calculate interaction energy between ligand and everything else in the atomgroup.
 * Exactly equals the intermolecular term (and its components) computed by AutoDock Vina.
 *
 * \param ag Atomgroup. Gradients are updated.
 * \param energy Energy value to return.
 * \param energies_detailed Per-component energy values to return, can be NULL. \c branch is set to 0.0.
 * \param vp Parameters of Vina potential. See \ref mol_vina_params_init.
 */
void mol_vina_energy_inter(
	struct mol_atom_group *ag,
	double *energy,
	struct mol_vina_components *energies_detailed,
	const struct mol_vina_params *vp);

/**
 * Initialize Vina potential.
 * \param ag Atomgroup.
 * \param lig_index_first Index of the first ligand atom.
 * \param lig_index_last Index of the last (inclusive) ligand atom.
 * \param ignore_topology If \c false, use covalent bond topology from atomgroup. If \c true, guess bonds based on distance between atoms (not very accurately).
 * \param box_buffer
 * \return Parameters of Vina potential.
 */
struct mol_vina_params mol_vina_params_init(
	const struct mol_atom_group *ag,
	const size_t lig_index_first,
	const size_t lig_index_last,
	const bool ignore_topology,
	const double box_buffer);

void mol_vina_params_destroy(struct mol_vina_params *vp);

/**
 * Scale all energy weights except branch by \p scale.
 *
 * Basically, scales total Vina energy.
 */
void mol_vina_components_scale(struct mol_vina_components *vc, const double scale);

/**
 * Find the box containing the ligand, then add a buffer and cutoff to all sides.
 * You should call \ref mol_vina_update_inter_list afterwards if you want to have
 * correct intermolecular energy (you probably do).
 *
 * During the simulations, it's a good idea to do something like
 *
 *     if (mol_vina_update_box(vp, ag, buf, true)) mol_vina_update_inter_list(vp, ag);
 *
 * \param vp Vina parameters. The cutoff and ligand atom indices are used; the box coordinates are saved here too.
 * \param ag Atomgroup.
 * \param buffer The buffer to add to all sides of the box, Angstroms.
 * \param lazy If \c true, only update the box if the ligand is outside the old one.
 * \return True if the box is updated (always the case if \p lazy is false).
 */
bool mol_vina_update_box(
	struct mol_vina_params *vp,
	const struct mol_atom_group *ag,
	const double buffer,
	const bool lazy);

/**
 * Update the list of the receptor atoms for \ref mol_vina_energy_inter.
 * It's a good idea to call it after \ref mol_vina_update_box if you want correct
 * intermolecular energy.
 * All the receptor atoms inside the box or within \c sqrt(vp->cutoff_sq) are included.
 *
 * \param vp Vina parameters.
 * \param ag Atomgroup.
 */
void mol_vina_update_inter_list(
	struct mol_vina_params *vp,
	const struct mol_atom_group *ag);

/**
 * Get the number of rotatable bonds from
 * \param pdbqt_filename
 * \return
 */
size_t mol_vina_get_num_branches_from_pdbqt(const char *pdbqt_filename);

/**
 * Calculate free energy from potential energy according to Eq. (9) in [1].
 *
 * \param energy Potential energy.
 * \param nbranch Number of active rotatable bonds between heavy atoms. Usually equal to the number of branches in PDBQT (see /ref mol_vina_get_num_branches_from_pdbqt).
 * \param vp Vina parameters. Can be set to NULL, in this case the default weights would be used.
 * \return Free energy.
 */
double mol_vina_free_energy(const double energy, const size_t nbranch, const struct mol_vina_params *vp);

#endif // _MOL_ENERGY_VINA_H_