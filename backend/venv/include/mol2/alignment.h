#ifndef _MOL_ALIGNMENT_H_
#define _MOL_ALIGNMENT_H_

#include "atom_group.h"

struct mol_alignment {
	char *seq1;
	char *seq2;
};

enum mol_traceback_direction
{
	MOL_T_LEFT,
	MOL_T_UP,
	MOL_T_DIAGONAL,
	MOL_T_ROOT
};

struct mol_traceback_entry {
	enum mol_traceback_direction dir;
	int score;
};

/* seq_columns is the sequence that is the label
 * for the columns. seq_rows is the label for the rows
 */
struct mol_traceback_matrix {
	struct mol_traceback_entry *entries;
	size_t ncolumns;
	size_t nrows;
	char *seq_columns;
	char *seq_rows;
};

struct mol_traceback_matrix *new_mol_traceback_matrix(char *seq_columns,
		char *seq_rows);
void destroy_mol_traceback_matrix(struct mol_traceback_matrix *matrix);
void free_mol_traceback_matrix(struct mol_traceback_matrix *matrix);
struct mol_alignment *global_alignment(struct mol_traceback_matrix *matrix);
struct mol_alignment *mol_align_to_seqres(struct mol_atom_group *ag);

void destroy_mol_alignment(struct mol_alignment *alignment);
void free_mol_alignment(struct mol_alignment *alignment);

#endif /* _MOL_ALIGNMENT_H_ */
