/** \file atom_group.h
        This file contains structures and functions
        for dealing with atom groups.
*/
#ifndef _MOL_ATOM_GROUP_H_
#define _MOL_ATOM_GROUP_H_


#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#include "vector.h"
#include "matrix.h"
#include "lists.h"
#include "hashes.h"
#include "geometry.h"
#include "khash.h"
#include "utils.h"

KHASH_DECLARE(RESI_HASH, struct mol_residue_id, struct mol_residue);

/**
 * Atom record type (relevant for atomgroups read in from PDB/MS files).
 */
enum mol_atom_record {
	MOL_RUNKNOWN,
	MOL_ATOM,
	MOL_HETATM
};

/**
 * Atom hydrogen bonding properties.
 */
enum mol_atom_hbond_props {
	UNKNOWN_HPROP = 0,       ///< Atom does not participate in any hydrogen bonds.
	HBOND_DONOR = 1,         ///< Atom is a donor.
	HBOND_ACCEPTOR = 2,      ///< Atom is an acceptor; it's hbond_base_atom_id is covalently linked heavy atom.
	DONATABLE_HYDROGEN = 4,  ///< Atom is a donatable hydrogen; it's hbond_base_atom_id is corresponding donor.
};


/** Add hydrogen bond property flag \p prop to atom \p atomi. */
#define MOL_ATOM_HBOND_PROP_ADD(ag, atomi, prop) (ag)->hbond_prop[atomi] |= (prop)
/** Set hydrogen bond property flag \p prop to atom \p atomi, discarding all flags set earlier. */
#define MOL_ATOM_HBOND_PROP_SET(ag, atomi, prop) (ag)->hbond_prop[atomi] = (prop)
/** Return non-zero if atom \p atomi has hydrogen bond property \p prop. It may or may not have other properties. */
#define MOL_ATOM_HBOND_PROP_IS(ag, atomi, prop) ((ag)->hbond_prop[atomi] & (prop))

/**
 * Atom residue index.
 * Each residue is identified by a {residue_id, chain_id, insertion} triplet.
 */
struct mol_residue_id {
	int16_t residue_seq;	/**< Residue number in chain (e.g. as read from pdb file).
					Missing value internal: 0. Missing value output: 0*/
	char chain;		/**< Residue chain.
					Missing value internal: "\0". Missing value output: "X"*/
	char insertion;		/**< Residue insertion code. See PDB format specification for details.
					Missing value internal: " ". Missing value output: " "*/
};

/**
 * Residue.
 * Provides quick access to the atoms of a particular residue in the atom group.
 * Atoms belonging to the residue are assumed to be laid out consecutively in the atomgroup arrays.
 */
struct mol_residue {
	struct mol_residue_id residue_id;

	size_t atom_start;
	size_t atom_end;
	size_t rotamer;
	char name[4];
};

/**
 * Represents a group of atoms.
 *
 * The fields of the atomgroup data structure can be roughly divided into
 * 2 types: atomic and other.
 *
 * * \b ATOMIC
 * type fields are arrays with the length equal to \c natoms,
 * which specify some property of each atom in the atomgroup.
 * Atomic fields can be approximately devided into several subgroups:
 *
 *   - [pdb] <br/>
 *     The core set of atomic type fields is used to store the properties
 *     contained in the ATOM/HETATM section of standard PDB files.
 *     Conversely, these fields are populated when the atomgroup is read from
 *     PDB/MS files. Some of these fields are initialized when the atom group
 *     is read from libmol-specific JSON files.
 *     \ref coords, \ref element, \ref atom_name, \ref residue_id,
 *     \ref residue_name, \ref occupancy, \ref B, \ref segment_id,
 *     \ref formal_charge, \ref record.
 *
 *   - [forcefield] <br/>
 *     PDB-type fields are supplemented by a set of fields describing some
 *     widespread classical forcefield parameters. These are initialized when
 *     the atomgroup is read from JSON files or by using
 *     mol_atom_group_read_geometry() or mol_atom_group_add_prms()
 *     on a pre-existing (e.g. read from PDB) atomgroup.
 *     \ref vdw_radius, \ref vdw_radius03, \ref eps, \ref eps03,
 *     \ref ace_volume, \ref charge, \ref hbond_props, \ref hbond_base_atom_id,
 *     \ref ftypen
 *
 *   - [various]  <br/>
 *     Finally, the remaining group of atomic fields is used to store some
 *     other useful atomgroup properties. Most of these were introduced to
 *     serve the needs of the PIPER docking program.
 *     \ref pwpot_id, \ref mask, \ref surface, \ref attraction_level,
 *     \ref backbone.
 *
 * * \b OTHER
 * type fields usually specify some property of the atomgroup as a whole.
 * These, too, form several subgroups:
 *
 *   - [geometry] <br/>
 *     This group of fields specifies the geometry elements and the related
 *     per-atom geometry lists:
 *     \ref bonds, \ref angles, \ref dihedrals, \ref impropers,
 *     \ref bond_lists, \ref angle_lists, \ref dihedral_lists,
 *     \ref improper_lists
 *
 *   - [residue-level] <br/>
 *     These fields deal with residue-level representation of the atom group:
 *     \ref residues, \ref residue_list, \ref seqres.
 *
 *   - [metadata] <br/>
 *     The metadata hash table forms a group of its own.
 *     It can be very useful as it allows to attach arbitrary data to the atom
 *     group.
 *     \ref metadata
 *
 *   - [active lists] <br/>
 *     A group of fields that serve the needs of the minimization procedure.
 *     These are usually constructed based on the \ref fixed atomic field
 *     as a complementary set of "non-fixed" degrees of freedom.
 *     Note: most of the libmol energy functions only compute interactions between
 *     active atoms.
 *
 * Each field of the atom group data structure should be a null pointer if the
 * data for that field is absent. All functions dealing with atom group should
 * correctly handle the situations when some fields of it are NULLs.
 *
 * For the atomic fields, which are arrays, there is also a possibility that a
 * portion (or all) of field elements cannot be assigned meaningful values
 * when performing some operations on the atom group. In order to be able to
 * handle such situations, the library provides a set of set of constants
 * specifying default values for the elements of atomic fields. These constants
 * are provided in the atom_group_missing.h header.
 * There are two types of such constants:
 *
 * The ``_none`` type constants are needed when the atom group field has to be
 * initialized based on partially available data.
 * For the atom group field with name ``fieldname``, the corresponding ``_none``
 * constant name is ``mol_atom_group_fieldname_none``
 *
 *     Example: Two atom groups are being joined. Ag1 has the ``occupancy`` field
 *     initialized, while Ag2 does not. In the resulting atom group,
 *     the atoms coming from Ag1 will have the value of the ``occupancy`` field set
 *     to the original value, while the atoms of Ag2 will have the value of the
 *     the same field set the value of the ``mol_atomgroup_occupancy_none`` constant.
 *
 * The ``_default`` type constants are primarily used when writing the
 * atomgroup to various file formats (PDB/MS/JSON), and represent the values
 * which will be assigned to the corresponding atomic properties in the file
 * if the field or a field element haven't been initialized.
 * For the atom group field with name ``fieldname``, the corresponding ``_default``
 * constant name is ``mol_atom_group_fieldname_default``
 *
 *     Example: An atomgroup has the "residue_name" field uninitialized or it is
 *     initialized, but some atoms have NULLs in this field. In this case,
 *     some or all atoms will have the residue name written according to the
 *     value of the ``mol_atomgroup_residue_name_default`` constant.
 *
 */
struct mol_atom_group {
	size_t natoms; //!< Number of atoms in the atom group.

	struct mol_vector3 *coords;	/**< Atomic coordinates. Array. Length: natoms.*/

	struct mol_vector3 *gradients;	/**< Atomic gradients. Array. Length: natoms.*/

	char **element; 		/**< Atomic elements. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms or json files.*/

	char **atom_name; 		/**< Atomic names. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms, json files.*/

	char *alternate_location; 	/**< Atomic alternative locations (see PDB format specification). Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms, json (optional) files.*/

	struct mol_residue_id *residue_id; /**< Atomic residue ids. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms, json (optional) files.*/

	char **residue_name;		/**< Atomic residue names. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms, json files.*/

	double *occupancy;		/**< Atomic occupancies. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms files.*/

	double *B;			/**< Atomic B-factors. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms files.*/

	char **segment_id;		/**< Atomic segment ids. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms files.*/

	char **formal_charge;		/**< Atomic formal charges. Array. Length: natoms. <br/>
						Initialized when ag is read from pdb, ms files.*/

	enum mol_atom_record *record;	/**< Atomic PDB record. Array. Length: natoms. <br/>
	 	 	 	 	 	Initialized when ag is read from from PDB, MS files.*/


	double *vdw_radius;		/**< Atomic VdW radii. Array. Length: natoms. <br/>
						Initialized when ag is read from json files <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag <br/>
						 or using mol_atom_group_add_prms() on a pre-existing ag.*/

	double *vdw_radius03;		/**< Atomic VdW radii for 0-3 bonded interactions. Array. Length: natoms. <br/>
						Initialized when ag is read from json files <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag.*/

	double *eps;			/**< Atomic VdW epsilon parameters. Array. Length: natoms. <br/>
						Initialized when ag is read from json files <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag.*/

	double *eps03;			/**< Atomic VdW epsilon parameters for 0-3 bonded interactions. Array. Length: natoms. <br/>
						Initialized when ag is read from json files <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag.*/

	double *ace_volume;		/**< Atomic ace volume parameters for GBSA potential. Array. Length: natoms. <br/>
						Initialized when ag is read from json files (optional field) <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag (if present in the parameter file).*/

	double *charge;			/**< Atomic charges. Array. Length: natoms. <br/>
						Initialized when ag is read from json files <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag <br/>
						 or using mol_atom_group_add_prms() on a pre-existing ag.*/


	enum mol_atom_hbond_props *hbond_prop;

	size_t *hbond_base_atom_id;

	int *pwpot_id;			/**< Atomic types for pairwise potential (e.g DARS). Array. Length: natoms. <br/>
						Initialized when ag is read from json files (optional field) <br/>
						 or using mol_atom_group_read_geometry() on a pre-existing ag (if present in the parameter file). <br/>
						 or using mol_atom_group_add_prms() on a pre-existing ag.*/

	bool *mask;			/**< Atomic mask. Array. Length: natoms. <br/>
						Masked atoms are not used in construction of certain piper grids (usually all except repulsive vdw grids). <br/>
						Initialized by using mol_atom_group_mask() on a pre-existing ag.*/

	bool *surface;			/**< Atomic surface accessibility flag. Array. Length: natoms. <br/>
						Surface atoms are treated differently when constructing certain piper grids. <br/>
						Initialized when ag is read from ms files <br/>
						 or by using compute_surface()/baccs()/accs() on a pre-existing ag.*/

	double *attraction_level;	/**< Atomic attraction level. Array. Length: natoms. <br/>
						Attraction level modifies the values of attvdw and point piper grids. <br/>
						Value of 0.0 corresponds to unmodified grids. <br/>
						Initialized when ag is read from ms files.*/

	bool *backbone;			/**< Atomic "backbone" flag. Array. Length: natoms.  <br/>
						Used in hbond potential. <br/>
						Initialized when ag is read from from JSON files <br/>
						 or manually.*/

	size_t *ftypen;			/**< Atomic types. Array. Length: natoms. <br/>
						Currently only used in GBSA potential calculations and when reading rtf/prm files. */

	size_t num_atom_types;			/**< Number of atom types (Length of the ftype_name array)*/
	char **ftype_name;			/**< Atom type names. Array. Length: num_atom_types. <br/>
							Currently only used in GBSA potential calculations and when reading rtf/prm files. */

	/** Atomic geometry (bonds, angles, dihedrals and impropers)
	 * can be initialized either by reading ag from json files,
	 * or using mol_atom_group_read_geometry() on a pre-existing ag.
	 */
	size_t nbonds;					/**< Number of bonds.*/
	struct mol_bond *bonds;				/**< Bonds array. Array. Length: nbonds.*/
	struct mol_index_list *bond_lists;		/**< Per-atom bond lists. Array. Length: natoms.*/

	size_t nangles;					/**< Number of angles.*/
	struct mol_angle *angles;			/**< Angles array. Array. Length: nangles.*/
	struct mol_index_list *angle_lists;		/**< Per-atom angle lists. Array. Length: natoms.*/

	size_t ndihedrals;				/**< Number of dihedrals.*/
	struct mol_dihedral *dihedrals;			/**< Dihedrals array. Array. Length: ndihedrals.*/
	struct mol_index_list *dihedral_lists;	/**< Per-atom dihedral lists. Array. Length: natoms.*/

	size_t nimpropers;				/**< Number of impropers.*/
	struct mol_improper *impropers;			/**< Impropers array. Array. Length: nimpropers.*/
	struct mol_index_list *improper_lists;	/**< Per-atom improper lists. Array. Length: natoms.*/


	size_t nresidues;			/**< Number of residues in the residue list**/
	struct mol_residue **residue_list;      /**< Residue list. Array. Length: nresidues. <br/>
							Initialized when ag is read from pdb, ms files <br/>
							 or using mol_atom_group_create_residue_list() on a pre-existing ag.*/

	kh_RESI_HASH_t *residues;		/**< Residue hash. Single object (not an array). <br/>
							Used to find residues by residue ids using find_residue(). <br/>
							Initialized when ag is read from pdb, ms files <br/>
							 or using mol_atom_group_create_residue_hash() on a pre-existing ag.*/

	char *seqres;				/**< Residue sequence. String. <br/>
							Initialized when ag is read from pdb, ms files (if SEQRES record is present) <br/>
							 or using mol_attach_pdb_seqres() on a pre-existing ag.*/

	kh_STR_t *metadata;	/**< Atom group metadata. See mol_atom_group_add_metadata() */

	// The following set of fields describes fixed and active (not fixed)
	// degrees of freedom in the atom group.
	//
	// These are managed by the
	// 	mol_fixed_init(),
	// 	mol_fixed_update(),
	// 	mol_fixed_update_with_list(),
	// 	mol_fixed_update_active_lists()
	// set of functions.
	//
	// Currently, only used in minimization.
	bool *fixed;					 /**< Atomic "fixed" flag. Array. Length: natoms. <br/>*/
	struct mol_index_list *active_atoms;		 /**< Active atoms list. Single object (not an array). <br/>*/
	struct mol_index_list *active_bonds; 		 /**< Active bonds list. Single object (not an array). <br/>*/
	struct mol_index_list *active_angles;		 /**< Active angles list. Single object (not an array). <br/>*/
	struct mol_index_list *active_dihedrals;	 /**< Active dihedrals list. Single object (not an array). <br/>*/
	struct mol_index_list *active_impropers;	 /**< Active impropers list. Single object (not an array). <br/>*/

};


struct mol_atom_group_list {
	size_t size;
	struct mol_atom_group *members;
};

// Use this macro to apply a macro to each of the fields of an atom_group
#define MOL_FOR_ATOMIC_FIELDS(macro, ag, ...) do {		\
		macro(ag, coords, ##__VA_ARGS__);		\
		macro(ag, gradients, ##__VA_ARGS__);		\
		macro(ag, element, ##__VA_ARGS__);		\
		macro(ag, atom_name, ##__VA_ARGS__);		\
		macro(ag, alternate_location, ##__VA_ARGS__);	\
		macro(ag, residue_id, ##__VA_ARGS__);		\
		macro(ag, residue_name, ##__VA_ARGS__);	\
		macro(ag, occupancy, ##__VA_ARGS__);		\
		macro(ag, B, ##__VA_ARGS__);			\
		macro(ag, segment_id, ##__VA_ARGS__);			\
		macro(ag, formal_charge, ##__VA_ARGS__);	\
		macro(ag, vdw_radius, ##__VA_ARGS__);		\
		macro(ag, vdw_radius03, ##__VA_ARGS__);	\
		macro(ag, eps, ##__VA_ARGS__);		\
		macro(ag, eps03, ##__VA_ARGS__);	\
		macro(ag, ace_volume, ##__VA_ARGS__);		\
		macro(ag, charge, ##__VA_ARGS__);		\
		macro(ag, pwpot_id, ##__VA_ARGS__);		\
		macro(ag, fixed, ##__VA_ARGS__);		\
		macro(ag, ftypen, ##__VA_ARGS__);		\
		macro(ag, ftype_name, ##__VA_ARGS__);		\
		macro(ag, mask, ##__VA_ARGS__);		\
		macro(ag, surface, ##__VA_ARGS__);		\
		macro(ag, attraction_level, ##__VA_ARGS__);	\
		macro(ag, backbone, ##__VA_ARGS__);		\
		macro(ag, record, ##__VA_ARGS__);		\
		macro(ag, hbond_prop, ##__VA_ARGS__);		\
		macro(ag, hbond_base_atom_id, ##__VA_ARGS__);	\
	} while(0)

#define mol_atom_group_init_atomic_field(ag, field) do {					\
		if (ag->field == NULL)					\
			ag->field = calloc(ag->natoms, sizeof(*(ag->field))); \
	} while(0)

/**
 * Create a new atom group or list of atom groups.
 * \return  Newly allocated atomgroup, all fields initialized to zero.
 */
struct mol_atom_group *mol_atom_group_create(void);
struct mol_atom_group_list *mol_atom_group_list_create(size_t n);

/**
 * Join two atom groups together.
 *
 * Most fields are simply concatenated.
 *
 * If the field is present in only one input atom_group, missing values are set to 0.
 * If the field is \c NULL in both \p A and \p B, it is not allocated and is set to \c NULL in the result.
 * Geometry lists get appropriate index adjustments.
 *
 * Atom types (ftypen) are shifted so they don't intersect between two molecules.
 * E.g., if \p A has 20 atom types, the atom types of B will be incremented by 20.
 * This creates some redundancy if we're joining two molecules with the same forcefield,
 * but safely handles joining two molecules with the different set of atom types: the
 * resulting ftypen would not have any collisions.
 *
 * \param A The first atomgroup to be joined.
 * \param B The second atomgroup to be joined.
 * \return The resulting atomgroup.
 */
struct mol_atom_group *mol_atom_group_join(
		const struct mol_atom_group *A,
		const struct mol_atom_group *B);

/**
 * Create a copy of an atom group data structure.
 * Copies all initialized fields of the original atom group except active lists.
 * All pointers to atom group field members are reassigned to the corresponding
 * field members of the new atom group.
 * Active lists are not copied, but rebuilt based on the *fixed* field of the
 * original atom group if it is not NULL.
 * Does not copy metadata.
 *
 * \param ag Atomgroup to be copied.
 * \return A copy of the original atomgroup.
 */
struct mol_atom_group *mol_atom_group_copy(const struct mol_atom_group *ag);

/**
 * Copies over as many atomic properties as possible from atom j in agB to atom i in agA.
 * Non-atomic properties (including per-atom geometry lists, etc.) are not copied.
 * The property is only copied if the corresponding field is initialized in both atomgroups.
 * It is the users responsibility to make sure that that i and j do not exceed the number of
 * atoms in the atomgroups.
 *
 * \param agA The recipient atom group.
 * \param i   The atom id of the recipient atom group.
 * \param agB The source atom group.
 * \param j   The atom id of the source atom group.
 */
void copy_atom(
	struct mol_atom_group *agA, size_t i,
	const struct mol_atom_group *agB, size_t j);

/**
 * Destroy the atomgroup, freeing all internal fields.
 *
 * \param ag The atom group to be destroyed.
 */
void mol_atom_group_destroy(struct mol_atom_group *ag);

/**
 * Destroy the atomgroup and free the atomgroup pointer.
 *
 * \param ag The atom group to be freed.
 */
void mol_atom_group_free(struct mol_atom_group *ag);
void mol_atom_group_list_destroy(struct mol_atom_group_list *ag_list);
void mol_atom_group_list_free(struct mol_atom_group_list *ag_list);

/**
 * Check if 2 residue ids are equal.
 * \return 1 if ids are equal and 0 otherwise.
 */
khint_t mol_residue_id_equal(
		struct mol_residue_id a,
		struct mol_residue_id b);

/**
 * Compare 2 residue ids.
 * Comparison is carried out in 3 stages the following order of properties:
 * chain -> residue_seq -> insertion
 * At each stage, the function compares the corresponding property
 * using standard comparators for the property data type and returns the
 * resulting value if it's not 0 (that is, if properties are not equal).
 * If compared properties are equal, the function continues to the next stage,
 * until all properties have been compared.
 * \return -1 if id a  < id b
 *          0 if id a == id b
 *          1 if id a  > id b.
 */
int mol_residue_id_cmp(
		const void *a,
		const void *b);

/**
 * Create \c ag->residues hashtable. If it already exists, the old one is discarded.
 * \param ag Atomgroup.
 * \return True on success, false on failure. The error message is printed to stderr.
 */
bool mol_atom_group_create_residue_hash(struct mol_atom_group *ag);

/**
 * Create \c ag->residue_list based on the \c ag->residues hashtable.
 * If it already exists, no action is taken.
 * \param ag Atomgroup.
 */
void mol_atom_group_create_residue_list(struct mol_atom_group *ag);

/**
 * Find residue in atom group by residue id
 * (a combination of chain id, residue sequence id and insertion).
 * Requires the atom group residue hash to be initialized.
 * \return A pointer to the residue if it is present in the residue hash table.
 *         NULL if the residue is not present of if residue hash is not initialized.
 */
struct mol_residue *find_residue(
		const struct mol_atom_group *ag,
		const char chain,
		const int16_t residue_seq,
		const char insertion);

/**
 * Find atom in atom group by residue id and atom name
 * (a combination of chain id, residue sequence id, insertion and atom name).
 * Requires the atom group residue hash to be initialized.
 * \return Atom index if it is present in the atom group.
 *         -1 if the atom is not present of if residue hash is not initialized.
 */
ssize_t mol_find_atom(
		const struct mol_atom_group *ag,
		const char chain,
		const int16_t residue_seq,
		const char insertion,
		const char* atom_name);

/**
 * Find atom residue.
 * This function is a wrapper over find_residue()
 * that uses atom residue id to find the corresponding residue.
 * \return A pointer to the residue if it is present in the residue hash table.
 *         NULL if the residue is not present,
 *           or if residue hash is not initialized,
 *           or if atom id is larger then the number of atom in the ag.
 */
struct mol_residue *mol_atom_residue(
		const struct mol_atom_group *ag,
		size_t atom_index);

/**
 * Print residues contained in the ag residue hash table to standard output.
 * Doesn't do anything if residue hash hasn't been initialized.
 */
void print_residues(const struct mol_atom_group *ag);

/**
 * Get the covalent partner of atom \p atom_i, corresponding to its bond number \p neighbor_i.
 * \param ag Atom group.
 * \param atom_i Index of atom.
 * \param neighbor_i Index of bond. Must be less than ag->bond_lists[atom_i].size
 * \return
 */
size_t mol_atom_group_get_neighbor(const struct mol_atom_group *ag, const size_t atom_i, const size_t neighbor_i);

/**
 * Update per-atom lists of bonds/angles/dihedrals/impropers. Must be called after each topology change.
 * Assumes that (a) ag->X_lists are already allocated, and (b) their sizes are correct.
 */
void mol_atom_group_fill_geometry_lists(struct mol_atom_group *ag);

/**
 * Add metadata to atomgroup. Metadata is just a \c void* pointer that "sticks" to atomgroup and
 * can be retrieved using mol_atom_group_fetch_metadata(). It can point to arbitrary data, be it single string,
 * per-atom parameters, or some kind of pairwise tabular potential. It can be used, e.g., for handling some data that's
 * highly specific to some energy function, so it does not warrant including it as a field in \ref mol_atom_group,
 * while still avoiding the hassle of passing this data around as an additional function parameter.
 *
 * Be warned: mol_atom_group_join(), mol_atom_group_free(), mol_atom_group_select(), etc do **not**
 * handle metadata at all. If you use any of those, you should handle metadata manually.
 * See mol_atom_group_join_atomic_metadata().
 *
 * \param ag Atomgroup.
 * \param key String that is used as a handle of this metadata. If metadata with such key already exists, it (the pointer)
 * will be silently replaced, with no deallocation or any other handling.
 * \param data Pointer of \c void* to the data. The data is not getting copied -- only this pointer is actually stored
 * in \p ag.
 */
void mol_atom_group_add_metadata(struct mol_atom_group *ag, const char *key, void *data);

/**
 * Return metadata (stored pointer). You will have to manually cast it to the original type.
 *
 * For more information about metadata see mol_atom_group_add_metadata().
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 * \return The stored pointer. \c NULL if no metadata with such \p key exists in \p ag.
 */
void *mol_atom_group_fetch_metadata(const struct mol_atom_group *ag, const char *key);

/**
 * Check if metadata with given key is present in atomgroup.
 *
 * For more information about metadata see mol_atom_group_add_metadata().
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 * \return True if metadata with \p key was added to \p ag; false otherwise.
 */
bool mol_atom_group_has_metadata(const struct mol_atom_group *ag, const char *key);

/**
 * Remove metadata from atomgroup. It only removes the stored \c void* pointer, but does not deallocate the memory.
 * If you want to avoid memory leaks, you are advised to call mol_atom_group_fetch_metadata() and \c free the
 * returned pointer, and only afterwards call mol_atom_group_delete_metadata().
 *
 * For more information about metadata see mol_atom_group_add_metadata().
 *
 * \param ag Atomgroup.
 * \param key Metadata handle.
 */
void mol_atom_group_delete_metadata(struct mol_atom_group *ag, const char *key);

/**
 * Helper function to join metadata when joining two atomgroups with mol_atom_group_join(). It works with any
 * metadata that has equally-sized contiguous-stored per-atom elements. E.g., the array of doubles or some
 * custom structs similar to \ref mol_atom_group.coords or \ref mol_atom_group.vdw_radius. This function does what
 * mol_atom_group_join() does for the default fields.
 *
 * It assumes that the metadata with handle \p key in \p A has \c A->natoms*element_size bytes; same with \p B.
 * Then the new memory chunk of \c ag->natoms*element_size bytes is allocated, and data from \p A and \p B is copied
 * to it.
 * If there is no metadata with handle \p key in either \p A or \p B, then the corresponding region of joined
 * metadata is set to 0s. If there's no metadata neither in \p A nor in \p B, then the new metadata is not created.
 *
 * This function is not called automatically. You should call it manually for each metadata record you want to process.
 *
 * For more information about metadata see mol_atom_group_add_metadata().
 *
 * \param ag The joined atomgroup, returned by \c mol_atom_group_join(A, B). Metadata is added to it.
 * \param A First atomgroup.
 * \param B Second atomgroup.
 * \param key Metadata handle.
 * \param element_size The size of individual element.
 */
void mol_atom_group_join_atomic_metadata(
		struct mol_atom_group *ag,
		const struct mol_atom_group *A,
		const struct mol_atom_group *B,
		const char *key,
		const size_t element_size);


/**
 * Select atoms that satisfy the specified criteria.
 * Only atomic fields are present in the resulting atom group.
 *
 * \param ag Source atom group
 * \param keep_if_true Selector function.
 * \param func_params Selector function parameters.
 *
 * \return A new atom group where only atoms which satisfy the specified
 *         criterion are kept.
 */
struct mol_atom_group *mol_atom_group_select(
		const struct mol_atom_group *ag,
		bool (*keep_if_true)(
			const struct mol_atom_group *ag,
			size_t index,
			void *params),
		void *func_params);

/**
 * Filter out atoms that satisfy the specified criteria.
 * Only atomic fields are present in the resulting atom group.
 *
 * \param ag Source atom group
 * \param keep_if_false Selector function.
 * \param func_params Selector function parameters.
 *
 * \return A new atom group where only atoms which do not satisfy the specified
 *         criterion are kept.
 */
struct mol_atom_group *mol_atom_group_filter(
		const struct mol_atom_group *ag,
		bool (*keep_if_false)(
			const struct mol_atom_group *ag,
			size_t index,
			void *params),
		void *func_params);


/**
 * Check which atoms satisfy the specified criteria
 *
 * \param ag Source atom group
 * \param test_func Criteria evaluating function.
 * \param func_params Criteria function parameters.
 * \return An array of bools where arr[i] is true if
 *         test_func(ag, i, func_params) is true, false otherwise.
 */
bool *mol_atom_group_test_foreach(
		const struct mol_atom_group *ag,
		bool (*test_func)(
			const struct mol_atom_group *ag,
			size_t index,
			void *params),
		void *func_params);

/**
 * Filter out the atoms of the specified atom type.
 *
 * \param ag Source atom group
 * \param type Atom type string.
 * \return A new atom group which contains all atoms of the original,
 *         except for those matching the specified atom type.
 *         Only atomic fields are carried over.
 */
struct mol_atom_group *mol_remove_atom_type(
		struct mol_atom_group *ag,
		const char *type);

/**
 * Extract the atoms of the specified atom type.
 *
 * \param ag Source atom group
 * \param type Atom type string.
 * \return A new atom group which contains only those atoms of the original
 *         that match the specified atom type.
 *         Only atomic fields are carried over.
 */
struct mol_atom_group *mol_extract_atom_type(
		struct mol_atom_group *ag,
		const char *type);

/**
 * Extract the atoms of the specified atom types.
 *
 * \param ag Source atom group
 * \param types Atom types list.
 * \return A new atom group which contains only those atoms of the original
 *         that match the specified atom types.
 *         Only atomic fields are carried over.
 */
struct mol_atom_group *mol_extract_atom_types(
		struct mol_atom_group *ag,
		const struct mol_list *types);

/**
 * Extract the atoms having the specified atom type prefix.
 * Atom type prefix is a string that is matched to the
 * starting potion of the atom_name string (not ftype_name!).
 * It has little to do with the traditional forcefield atom types.
 * Atom type prefix can be used as a proxy of element name.
 *
 * \param ag Source atom group
 * \param type Atom type prefix.
 * \return A new atom group which contains only those atoms of the original
 *         that match the specified atom type prefix.
 *         Only atomic fields are carried over.
 */
struct mol_atom_group *mol_extract_atom_type_prefix(
		struct mol_atom_group *ag,
		const char *type);

/**
 * Extract the specified chain.
 *
 * \param ag Source atom group
 * \param chain Chain id.
 * \return A new atom group which contains only those atoms matching the
 *         specified chain id.
 *         Only atomic fields are carried over.
 */
struct mol_atom_group *mol_extract_chain(
		const struct mol_atom_group *ag,
		const char chain);

/**
 * Check whether the atom is a backbone atom.
 * Follows standard backbone convention:
 * only regular backbone heavy atoms pass (N, CA, C, O).
 * Backbone hydrogens and terminal groups are excluded.
 * This function is intended to be used with
 * mol_atom_group_select/filter/test_foreach.
 *
 * \param ag Source atom group.
 * \param index Atom index
 * \param params Dummy argument. Always NULL.
 */
bool mol_is_backbone(
		const struct mol_atom_group *ag,
		size_t index,
		void *params);

/**
 * Check whether the atom is a side chain atom.
 * All atoms that are not "extended backbone"
 * are considered side chain.
 * "Extended backbone" includes
 * N, CA, C, O, H, HT1, HT2, HT3, OXT.
 * This function is intended to be used with
 * mol_atom_group_select/filter/test_foreach.
 *
 * \param ag Source atom group.
 * \param index Atom index
 * \param params Dummy argument. Always NULL.
 */
bool mol_is_sidechain(
		const struct mol_atom_group *ag,
		size_t index,
		void *params);

/**
 * Initialize the atomgroup fields needed for energy evaluation and energy
 * minimization. These include \ref mol_atom_group.gradients and various fields
 * related to fixed and active degrees of freedom:
 * \ref mol_atom_group.fixed,
 * \ref mol_atom_group.active_atoms,
 * \ref mol_atom_group.active_bonds,
 * \ref mol_atom_group.active_angles,
 * \ref mol_atom_group.active_dihedrals,
 * \ref mol_atom_group.active_impropers.
 *
 * All the fields are initialized to their default values
 * (\c fixed and \c gradients are arrays of 0/0.0
 * and active lists are empty lists)
 *
 * \param ag The target atomgroup.
 */
void mol_fixed_init(struct mol_atom_group *ag);

/**
 * Update active lists based on the contents of \ref mol_atom_group.fixed field.
 * The active lists include
 * \ref mol_atom_group.active_atoms,
 * \ref mol_atom_group.active_bonds,
 * \ref mol_atom_group.active_angles,
 * \ref mol_atom_group.active_dihedrals,
 * \ref mol_atom_group.active_impropers.
 *
 * It is the users responsibility to make sure all these fields were initialized
 * (using mol_fixed_init) before running this function.
 *
 * \param ag The target atomgroup.
 */
void mol_fixed_update_active_lists(struct mol_atom_group *ag);

/**
 *  Update the update contents of the  \ref mol_atom_group.fixed field based on the
 *  user-provided list of fixed atom indices and run mol_fixed_update_active_lists().
 *
 *  \param ag The target atomgroup.
 *  \param nlist The size of the list.
 *  \param list Array of atom indices of length nlist. The specified atoms will be fixed.
 */
void mol_fixed_update(
		struct mol_atom_group* ag,
		size_t nlist,
		size_t* list);

/**
 *  Update the update contents of the  \ref mol_atom_group.fixed field based on the
 *  user-provided list of fixed atom indices and run mol_fixed_update_active_lists().
 *
 *  This function is a wrapper over mol_fixed_update().
 *
 *  \param ag The target atomgroup.
 *  \param fixed_list A list of atom indices.
 */
void mol_fixed_update_with_list(
		struct mol_atom_group* ag,
		const struct mol_index_list* fixed_list);

/**
 * Copy the coordinates of active (non-fixed) atoms from
 * \ref mol_atom_group.coords to a dense array.
 * It is the users responsibility to make sure the array has been
 * preallocated to a sufficient size.
 * This function is used in the minimization protocol.
 *
 * \param xyz A dense array of active atom coordinates. XYZ coordinates of each active atom follow each other.
 * \param ag The target atom group.
 */
void mol_atom_group_get_actives(
		double * restrict xyz,
		const struct mol_atom_group * restrict ag);

/**
 * Copy the coordinates of active (non-fixed) atoms from
 * a dense array to \ref mol_atom_group.coords.
 * It is the users responsibility to make sure the array has been
 * preallocated to a sufficient size.
 * This function is used in the minimization protocol.
 *
 * \param xyz A dense array of active atom coordinates. XYZ coordinates of each active atom follow each other.
 * \param ag The target atom group.
 */

void mol_atom_group_set_actives(
		struct mol_atom_group * restrict ag,
		const double * restrict xyz);

/**
 * Set the ref mol_atom_group.gradients to zeroes.
 * It is the users responsibility to make sure that the
 * ref mol_atom_group.gradients field is initialized.
 *
 * \param ag The target atom group.
 */
void mol_zero_gradients(struct mol_atom_group *ag);

/**
 * Get the centroid, which is the average of all the coordinates.
 *
 * \param[out] center Buffer for the result.
 * \param[in] ag Atomgroup.
 */
void centroid(struct mol_vector3 *center, const struct mol_atom_group *ag);

/**
 * Get the center of extrema, which is the average of the
 * max and min in each of the 3 dimensions
 *
 * \param[out] center Buffer for the result.
 * \param[in] ag Atomgroup.
 */
void center_of_extrema(
		struct mol_vector3 *center,
		const struct mol_atom_group *ag);

/**
 * Get the maximum distance between any two atoms in the atomgroup.
 *
 * \param[in] ag Atomgroup.
 * \return The largest distance.
 */
double mol_atom_group_diameter(const struct mol_atom_group *ag);

/**
 * Get the bounding box for the atomgroup.
 *
 * \param[out] min Bounding box min coordinate.
 * \param[out] max Bounding box min coordinate.
 * \param[in] ag Atomgroup.
 */
void mol_atom_group_bounding_box(
		struct mol_vector3 *min,
		struct mol_vector3 *max,
		const struct mol_atom_group *ag);

/**
 * Get the bounding sphere for the atomgroup.
 * Sphere center and radius are computed using a mixed implementation of
 * bouncing ball and Ritter's algorithm.
 *
 * \param[out] center Bounding sphere center coordinate.
 * \param[in] ag Atomgroup.
 * \return Bounding sphere radius.
 */

double mol_atom_group_bounding_sphere(
		struct mol_vector3 *center,
		const struct mol_atom_group *ag);

/**
 * Translate all atoms in \p ag.
 */
void mol_atom_group_translate(
		struct mol_atom_group *restrict ag,
		const struct mol_vector3 *restrict tv);

/**
 * Rotate all atoms in \p ag.
 */
void mol_atom_group_rotate(
		struct mol_atom_group *restrict ag,
		const struct mol_matrix3 *restrict r);

/**
 * Rotate and then translate all atoms in \p ag.
 */
void mol_atom_group_move(
		struct mol_atom_group *restrict ag,
		const struct mol_matrix3 *restrict r,
		const struct mol_vector3 *restrict tv);

void mol_atom_group_move_in_copy(
		const struct mol_atom_group *src,
		struct mol_atom_group *dest,
		const struct mol_matrix3 *r,
		const struct mol_vector3 *tv);

bool *mol_gaps(struct mol_atom_group *ag);

//deprecated
struct mol_atom_group *new_mol_atom_group(void) __attribute__ ((deprecated));
struct mol_atom_group_list *new_mol_atom_groups(size_t n) __attribute__ ((deprecated));
void destroy_mol_atom_group(struct mol_atom_group *ag) __attribute__ ((deprecated));
void free_mol_atom_group(struct mol_atom_group *ag) __attribute__ ((deprecated));
void destroy_mol_atom_group_list(struct mol_atom_group_list *ag_list) __attribute__ ((deprecated));
void free_mol_atom_group_list(struct mol_atom_group_list *ag_list) __attribute__ ((deprecated));
void zero_grads(struct mol_atom_group *ag) __attribute__ ((deprecated));



#endif /* _MOL_ATOM_GROUP_H_ */
