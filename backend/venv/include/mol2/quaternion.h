#ifndef _MOL_QUATERNION_H_
#define _MOL_QUATERNION_H_

#include "matrix.h"
#include "vector.h"


struct mol_quaternion {
	double W;
	double X;
	double Y;
	double Z;
};


#define MOL_QUATERNION_MULT_SCALAR(DST, U, C) do \
	{                                        \
		(DST).W = (U).W * (C);           \
		MOL_VEC_MULT_SCALAR(DST, U, C);  \
	} while(0)


#define MOL_QUATERNION_ADD(DST, U, V) do \
	{                                \
		(DST).W = (U).W + (V).W; \
		MOL_VEC_ADD(DST, U, V);  \
	} while(0)


#define MOL_QUATERNION_SUB(DST, U, V) do \
	{                                \
		(DST).W = (U).W - (V).W; \
		MOL_VEC_SUB(DST, U, V);  \
	} while(0)


/**
 * Allocate memory for one mol quaternion structure, and nullify the contents.
 *
 * \return Pointer to a newly allocated single quaternion.
 */
struct mol_quaternion *mol_quaternion_create(void);

/**
 * Create mol_quaternion_list of length \p n.
 *
 * This function creates a new \ref mol_quaternion_list, allocates memory so it can hold \p n \ref mol_quaternion
 * members, and sets \c list->size to \p n. Allocated quaternions are initialized to 0.
 *
 * \param n Size of list to allocate.
 * \return New \ref mol_quaternion list, allocated and initialized.
 */
struct mol_quaternion_list *mol_quaternion_list_create(const size_t n);

/**
 * Load list of quaternions from file and normalize them.
 *
 * File should contain a single quadruplet of quaternion components (W X Y Z) per line, with any whitespace separators.
 */
struct mol_quaternion_list *mol_quaternion_list_from_file(const char *filename);

/**
 * Load list of quaternions from file. Same as \ref mol_quaternion_list_from_file, but without normalization.
 */
struct mol_quaternion_list *mol_quaternion_list_from_file_unnormalized(const char *filename);

void mol_quaternion_free(struct mol_quaternion *q);

/**
 * Calculate the length (L2-norm) of quaternion \p q.
 */
double mol_quaternion_length(const struct mol_quaternion q);

/**
 * Calculate the square of length (L2-norm) of quaternion \p q.
 */
double mol_quaternion_length_sq(const struct mol_quaternion q);
/**
 * Normalize quaternion \p q, and return corresponding unit quaternion.
 */
struct mol_quaternion mol_quaternion_normalize(const struct mol_quaternion q);

/**
 * Convert quaternion \p q to 3*3 rotation matrix \p m.
 */
void mol_matrix3_from_quaternion(struct mol_matrix3 *m, const struct mol_quaternion q);

/**
 * Convert 3*3 rotation matrix \p m to quaternion \p q.
 */
void mol_quaternion_from_matrix3(struct mol_quaternion *q, const struct mol_matrix3 m);


/**
 * Calculate quaternion describing rotation around \p axis by \p angle.
 *
 * \param q[out] Pointer to the \c mol_quaternion where the result will be stored.
 * \param axis[in] Rotation axis. Must be unit vector.
 * \param angle[in] Angle in radians.
 */
void mol_quaternion_from_axis_angle(struct mol_quaternion *q, const struct mol_vector3 *axis, const double angle);

/**
 * Return axis and angle of the rotation from the quaternion.
 * Out of two possible rotation axes, the one used in XYZ coordinated of \p q is used.
 * For zero-angle rotation, the \p axis is set to some unspecified value (but still has unit length).
 *
 * \param axis[out] Rotation axis. Will be unit vector.
 * \param angle[out] Rotation angle in [-pi; pi] range.
 * \param q[in] Quaternion.
 */
void mol_quaternion_to_axis_angle(struct mol_vector3 *axis, double *angle, const struct mol_quaternion *q);

/**
 * Scale quaternion \p q (i.e., multiply all its components) by \p a.
 */
struct mol_quaternion mol_quaternion_scale(const struct mol_quaternion q, const double a);

/**
 * Calculate the product of \p qa and \p qb.
 */
struct mol_quaternion mol_quaternion_prod(const struct mol_quaternion qa, const struct mol_quaternion qb);

/**
 * Conjugate quaternion \p q.
 */
struct mol_quaternion mol_quaternion_conj(const struct mol_quaternion q);

/**
 * Find the inverse of quaternion \p q.
 */
struct mol_quaternion mol_quaternion_inv(const struct mol_quaternion q);


// Deprecated functions
void mol_quaternion_scaled(const struct mol_quaternion *q, const double a, struct mol_quaternion *q_scaled) __attribute__ ((deprecated));
void mol_quaternion_product(const struct mol_quaternion *qa, const struct mol_quaternion *qb, struct mol_quaternion *product) __attribute__ ((deprecated));
void mol_quaternion_conjugate(const struct mol_quaternion *q, struct mol_quaternion *qc) __attribute__ ((deprecated));
void mol_quaternion_inverse(const struct mol_quaternion *q, struct mol_quaternion *q_inv) __attribute__ ((deprecated));
void mol_quaternion_norm(struct mol_quaternion *p, const struct mol_quaternion q) __attribute__ ((deprecated));

#endif /* _MOL_QUATERNION_H_ */
