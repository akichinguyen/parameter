#ifndef _MOL_RMSD_H_
#define _MOL_RMSD_H_

#include "lists.h"
#include "vector.h"
#include "atom_group.h"

/**
 * \file rmsd.h
 */

/**
 * Compute RMSD between two arrays of \ref mol_vector3.
 *
 * This function takes in \ref mol_vector3 pointers \p la and \p lb, as well as
 * the \p len parameter which specifies the amount of vectors we are going to deal with.
 * It is worth mentioning that each of these data structures represents a vector pointing
 * to a specific atom in a protein structure. This way the function works by computing
 * the distance between all of these atom representations (ie: vectors) and then finishes
 * by returning the root-mean-square distance of all of the atoms in the protein, which
 * effectively is the distance between the two proteins.
 */
double rmsd(struct mol_vector3 *la, struct mol_vector3 *lb, size_t len);

/**
 * Compute RMSD between two atomgroups, which contain the same number of atoms in the same order.
 *
 * This following function utilizes the previous \ref rmsd function but instead operates on a
 * \ref mol_atom_group. However, we have the same logic here, since each atom is represented as
 * a 3D coordinate (i.e., \ref mol_vector3). The inputs are the two atom groups pointers that
 * will be compared, \p pa and \p pb. It should be noted that each of these groups must
 * contain the **same number of atoms** or the function will return \c NAN. Otherwise, the
 * function will return a root-mean-square distance between each of the atom groups.
 */
double atom_group_rmsd(struct mol_atom_group *pa, struct mol_atom_group *pb);

/**
 * Computer RMSD between subsets of two atom groups.
 *
 * This function has the same logic as the \ref rmsd function. The inputs are again two
 * pointers to \ref mol_vector3 \p la and \p lb. However, the third input is different, and
 * it is a \ref mol_index_list pointer \p indices. This effectively calculates the distance
 * between **specific** atoms in the protein dependent on which atoms are specified by the
 * indices in the index list. The function returns the root-mean-square distance between
 * the atoms at the indices specified.
 */
double index_rmsd(struct mol_vector3 *la, struct mol_vector3 *lb,
		  struct mol_index_list *indices);

/**
 * Create \ref mol_index_list containing indices of all atoms in atom group.
 *
 * This function creates an index list from an atom group. The only input is a
 * \ref mol_atom_group pointer \p ag. From this atom group, the function allocates space
 * for the index list matching the dimensions of the atom_group, and then incrementally
 * adds 1,2,3,...,natoms to the index list. The function then returns a pointer to this
 * newly constructed \ref mol_index_list.
 */
struct mol_index_list *atom_group_all_indices(struct mol_atom_group *ag);

struct mol_symmetry_list *mol_detect_symmetry(struct mol_atom_group *ag);

double mol_atom_group_rmsd_sym(struct mol_atom_group *pa, struct mol_atom_group *pb,
		struct mol_symmetry_list *symmetry);

#endif /* _MOL_RMSD_H_ */
