#ifndef _SMP_HBOND_H_
#define _SMP_HBOND_H_

/**
 * \file hbond
 * \brief This file contains functions for calculating hydrogen bond potential.
 *
 * It is separate from \ref energy.h due to its size.
 *
 * \addtogroup hbond
 * \brief Module to set up and calculate the hydrogen bond potential.
 *
 * The implementation is similar to the one used in Rosetta software suite, and is based on following publications:
 *
 * [1] Kortemme, T. et al. 2003. An orientation-dependent hydrogen bonding potential improves prediction of specificity and structure for proteins and protein-protein complexes. Journal of Molecular Biology. 326, 4 (2003), 1239–1259. DOI:https://doi.org/10.1016/S0022-2836(03)00021-4.
 * [2] Wedemeyer, W.J. and Baker, D. 2003. Efficient minimization of angle-dependent potentials for polypeptides in internal coordinates. Proteins: Structure, Function and Genetics. 53, 2 (2003), 262–272. DOI:https://doi.org/10.1002/prot.10525.
 *
 * Kortemme (2003) describe the basic potential idea, while Wedemeyer (2003) expand on the use of polynomial approximations.
 * If you want to understand the notation for angles etc., please read Kortemme (2003), or at least take a look at Fig. 1 there.
 * If you want to understand different bond types etc.,  please read Wedemeyer (2003).
 *
 * @{
 */

#include <mol2/atom_group.h>
#include <mol2/prms.h>
#include <mol2/nbenergy.h>

/** Metadata key for accessing second base atom id for acceptors that are part of aromatic rings */
#define SMP_HBOND_BASE2_METADATA_KEY "hbond_base2"
#define SMP_HBOND_BASE2_MISSING SIZE_MAX

/**
 * Update hbond bases. Must be called before calculating hbond parameters, and after loading molecular parameters
 * (\ref mol_atom_group_add_prms), geometry (\ref mol_atom_group_read_geometry), and hybridization parameters
 * (\ref smp_hybridization_read).
 *
 * In particular, it updates the base atoms for acceptors, which are not always correctly (if at all) listed in PSF.
 * The bases of hydrogens are not affected, but are verified.
 *
 * \param ag Atomgroup.
 * \param log File descriptor for printing results of verification, typically \c stderr. Set to \c NULL to silence warnings.
 * \return True if all atoms have appropriate bases, false if note.
 */
bool smp_hbond_init_bases(struct mol_atom_group *ag, FILE *log);


/**
 * Calculate hydrogen bond energy and gradients for \p ag.
 *
 * If \p use_split is \c true, only consider bonds between two subgroups of atoms:
 * those with \c id < \p atom_split and those with \c id > \p atom_split.
 *
 * \p use_split Can be useful if you have two molecules in \p ag, and want to calculate energy only between them.
 *
 * \param ag Atomgroup; gradients are updated.
 * \param energy Pointer to location where resulting energy is stored.
 * \param nblst Non-bonded list (typicall from \c agsetup.nblst).
 * \param weight Energy weight.
 * \param use_split Should we only calculate energies between two subgroups?
 * \param atom_split Atom ID (zero-based) at which split is performed. Typically, the first atom of second molecule. Ignored if \p use_split is \c false.
 */
void smp_energy_hbond(
	struct mol_atom_group *ag,
	double *energy,
	const struct nblist *nblst,
	const double weight,
	const bool use_split,
	const int atom_split);

/** @}*/
#endif /** _SMP_HBOND_H_ **/
