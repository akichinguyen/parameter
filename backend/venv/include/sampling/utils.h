#ifndef _SMP_UTILS_H_
#define _SMP_UTILS_H_

#include <stdlib.h>
#include <mol2/atom_group.h>

/**
 * \addtogroup utils Utils
 * \brief common functions and constants.
 * @{
 */

// This solution uses small hack: https://stackoverflow.com/q/5588855/929437, but it seems to be widely supported
// It also relies on __FILENAME__ macro defined in CMakeLists.txt
#define FPRINTF_ERROR(fd, fmt, ...) \
	fprintf(fd, "[Error] in function %s (%s:%d): " fmt "\n", __func__, __FILENAME__, __LINE__, ##__VA_ARGS__)
#define PRINTF_ERROR(fmt, ...) FPRINTF_ERROR(stderr, fmt, ##__VA_ARGS__)

#ifndef M_PI
static const double M_PI = 3.14159265358979323846;
#endif

/**
 * Copy substring from \p src to \p dst up to first whitespace, completely ignoring leading whitespaces.
 *
 * Example: "  cat dog " -> "cat"
 *
 * \param dst Destination string.
 * \param src Source string.
 * \param n Size of destination buffer. If the string is too long, it will be truncated and the null-terminator will be put.
 */
void smp_str_trim_and_copy(char *dst, const char *src, const size_t n);


//! Multiply VdW radii of all atoms by \p scale.
void smp_scale_vdw_radius(struct mol_atom_group *ag, const double scale);

//! Multiply matrix \p Z by scalar \p c, inplace.
void smp_matrix_mult_scalar(struct mol_matrix3 *Z, const double c);

//! Multiply first three matrices of \p Z by scalar \p c, inplace.
void smp_matrix3_mult_scalar(struct mol_matrix3 *Z, const double c);

//! For given two matrix X and Y will calculate sum of them
void smp_matrix_sum(struct mol_matrix3 *Z, const struct mol_matrix3 X, const struct mol_matrix3 Y);

//! Sets all gitven matrix elements to a given constant
void smp_matrix_set_scalar(struct mol_matrix3 *Z, const double c);

//! Copy VdW radii from \p ag to \p array. See also: \ref smp_vdw_radius_from_array.
void smp_vdw_radius_to_array(double *array, const struct mol_atom_group *ag);

//! Copy VdW radii from \p array to \p ag. See also: \ref smp_vdw_radius_to_array.
void smp_vdw_radius_from_array(struct mol_atom_group *ag, const double *array);

/**
 * Set ag->backbone according to atom names.
 *
 * Only the following atoms are considered part of the backbone: CA, C, N, O, H.
 */
void smp_atom_group_init_backbone_by_name(struct mol_atom_group *ag);

/**
 * Translate atoms from \p atom_list by vector \p t, and don't touch any other atom.
 * \param ag Atomgroup.
 * \param t Translation vector.
 * \param atom_list List of indices of atoms to be translated.
 */
void smp_atom_list_translate(
	struct mol_atom_group *ag,
	const struct mol_vector3 *t,
	const struct mol_index_list atom_list);

/**
 * Rotate atoms from \p atom_list by quaternion \p q around \p center, and don't touch any other atom.
 * \param ag Atomgroup.
 * \param q Rotation quaternion.
 * \param center Rotation center.
 * \param atom_list List of indices of atoms to be rotated.
 */
void smp_atom_list_rotate(
	struct mol_atom_group *ag,
	const struct mol_quaternion *q,
	const struct mol_vector3 *center,
	const struct mol_index_list atom_list);

/**
 * Find geometric center of subset of atomgroup.
 * \param ag Atomgroup.
 * \param atom_list List of indices of atoms.
 * \return Geometric center of atoms from \p atom_list.
 */
struct mol_vector3 smp_atom_list_centroid(
	const struct mol_atom_group *ag,
	const struct mol_index_list atom_list);

/**
 * Calculate a list of residues forming interface between the ligand and the receptor. The list is sorted by residue number.
 *
 * The ligand is specified by its first and last atoms; everything else in \p ag is considered to be a receptor.
 * The interface residue is the residue of the ligand/receptor that is in contact with any other residues of receptor/ligand, respectively.
 * Two residues are considered to be in contact if they have at least one pair of atoms within \p cutoff.
 *
 * \param interface_residues Return value. If \p alloc is \c false, \c interface_residues->members must have enough space for the result.
 * \param ag Atomgroup.
 * \param first_ligand_atom First ligand atom.
 * \param last_ligand_atom Last ligand atom (inclusive).
 * \param cutoff Cutoff, Angstrom.
 * \param alloc If \c true, allocate the \c interface_residues->members array to hold all elements returned (and no more). If \c false, assume that the array is already big enough; neither expand, nor shrink it.
 */
void smp_create_interface_residues_list(
	struct mol_index_list *interface_residues,
	const struct mol_atom_group* ag,
	const size_t first_ligand_atom,
	const size_t last_ligand_atom,
	const double cutoff,
	const bool alloc);

/**
 * Get list of all atoms with given element (\c ag->element). The list is sorted.
 * \param atom_list Return value. If \p alloc is \c false, \c atom_list->members must have enough space for the result.
 * \param ag Atomgroup.
 * \param element Element name. E.g., "H". Must not be longer than 10 bytes.
 * \param alloc If \c true, allocate the \c atom_list->memebers array to hold all elements returned (and no more). If \c false, assume that the array is already big enough; neither expand, nor shrink it.
 */
void smp_get_atom_list_by_element(
	struct mol_index_list *atom_list,
	const struct mol_atom_group *ag,
	const char *element,
	const bool alloc);

/**
 * If atom is in \p atom_list, mark it not fixed; otherwise, mark it fixed. The inverse of \c mol_fixed_update_with_list.
 * \param ag Atomgroup.
 * \param atom_list List of atoms to unfreeze.
 */
void smp_freeze_all_unfreeze_from_list(struct mol_atom_group *ag, const struct mol_index_list atom_list);

/**
 * Copy coordinates of atoms from \p ag to array \p dst.
 *
 * If \p atom_list is not \c NULL, only atoms from \p atom_list are copied.
 * \p dst should be large enough to contain copied data.
 *
 * \param dst Destination array.
 * \param ag Source atomgroup.
 * \param atom_list List of atoms to copy. If \c NULL, all atoms from \p ag are copied. Refers to the atom numbers in \p ag: atoms are written consecutively to \p dst.
 */
void smp_coords_to_array(
	struct mol_vector3 *dst,
	const struct mol_atom_group *ag,
	const struct mol_index_list *atom_list);

/**
 * Copy coordinates of atoms from array \p src to atomgroup \p ag.
 *
 * If \p atom_list is not \c NULL, only atoms from \p atom_list are copied.
 * \p dst should be large enough to contain copied data.
 *
 * \param ag Destination atomgroup.
 * \param src Source array.
 * \param atom_list List of atoms to copy. If \c NULL, all atoms in \p ag are copied.  Refers to the atom numbers in \p ag: atoms are read consecutively from \p src.
 */
void smp_coords_from_array(
	struct mol_atom_group *ag,
	const struct mol_vector3 *src,
	const struct mol_index_list *atom_list);

/**
 * Return \c true if any of the atom coordinates in \p ag are NaN or Inf.
 */
bool smp_atom_group_has_nan_coordinates(const struct mol_atom_group *ag);

/** @}*/
#endif /* _SMP_UTILS_H_ */
