#ifndef _SMP_TRANSFORM_EXP_H_
#define _SMP_TRANSFORM_EXP_H_

#include <mol2/vector.h>
#include <mol2/matrix.h>
#include <mol2/quaternion.h>

/**
 * \addtogroup l_transform_exp ExponentialCoordinates
 * \brief Module to deal with axis-angle representation of rigid body rotations.
 *
 * This module provides functionality for conversion between rotation matrix and exponential map (axis-angle) representations of rotational states.
 * Exponential maps are heavily used in the Rigforest module, as they provide continuous parametrization of the rotation group
 * and therefore allow to preform local minimization in the rigid body/torsional space without the need to introduce restraints.
 *
 * Currently, this module allows to:
 * - Construct rotation matrix from exponential map (axis-angle) representation:
 *  -# \ref smp_rmatr_from_exp()
 * - Compute partial derivatives of a rotation matrix with respect to rotation angle for the case of fixed rotation axis:
 *  -# \ref smp_rmatr_derive_wrt_theta()
 * - Compute partial derivatives of a rotation matrix with respect to exponential map coordinates:
 *  -# \ref smp_rmatr_derive_wrt_exp()
 * - Convert between exponential maps and quaternion representations:
 *  -# \ref smp_quaternion_from_exp() and \ref smp_quaternion_to_exp()
 *
 * The axis-angle representation, also known as exponential coordinates, is simply the unit-vector w of rotation axis
 * multiplied by the rotation angle theta.

 * It's relation to quaternions are straightforward: Q = { cos(theta/2); w sin(theta/2) }
 *
 * The expression for the rotation matrix is a bit more complicated, and is called Rogdrigues formula:
 *
 *     ( x*x  x*y  x*z )                        ( 1 0 0 )                  (  0  -z  +y )
 * R = ( x*y  y*y  y*z ) * (1 - cos(theta))  +  ( 0 1 0 ) * cos(theta)  +  ( +z   0  -x ) * sin(theta)
 *     ( x*z  y*z  z*z )                        ( 0 0 1 )                  ( -y  +x   0 )
 *
 * Here, (x, y, z) are components of unit rotation axis vector w, and theta is rotation angle.
 * Shortly, this can be written as:
 * R = A(w) * (1 - cos(theta)) + I * cos(theta) + S(w) * sin(theta).
 * The first two components (A and I) are symmetric and the third one (S) is skew-symmetric.
 * Also, S(w)^2 = A(w) - I.
 * This allows us to write R = S^2 * (1 - cos(theta)) + S * sin(theta) + I.
 *
 * In this module, we use the letter "w" to refer to the vector corresponding to the axis, and "v" to refer
 * to the exponential map. While they are collinear, they have very different meaning.
 *
 * Corollary: S(v) = S(w) * theta; S(w) = S(v) / |v|. Unless noted, plain S refers to S(w).
 * Warning: S(w)^2 = A(w) - I, but S(v)^2 = A(v) - theta^2 * I.
 * @{
 */


/**
 * Build a rotation matrix corresponding to the given exponential coordinates (axis-angle representation) using the Rodrigues formula.
 * \param r[out] Pointer to a preallocated mol_matrix3 that will contain the resultant rotation matrix.
 * \param v[in] Exponential coordinates.
 */
void smp_rmatr_from_exp(struct mol_matrix3 *r, const struct mol_vector3 v);

/**
 * Compute the partial derivatives of a rotation matrix \c r corresponding to rotation by angle \p theta around \p axis.
 * with respect to \p theta.
 * \param dr_over_dt[out] Pointer to a preallocated mol_matrix3 that will contain the result.
 * \param axis[in] Rotation axis. Normalization does not matter. If equal to zero, then all-zero matrix will be silently returned.
 * \param theta[in] Rotation angle.
 */
void smp_rmatr_derive_wrt_theta(
	struct mol_matrix3 *dr_over_dt,
	const struct mol_vector3 axis,
	const double theta);

/**
 * Compute the partial derivatives of a rotation matrix \c r with respect to the exponential map \p v.
 * Note that the result is a 3D tensor, in practice stored as a set of three 3x3 matrices.
 * The output is such that dr_over_dv[i].mJK = d(R.mJK) / dv[i] for i=0..2, J,K=1..3.
 *
 * Note: unlike all other functions, here the results for exp. maps w*theta and v*(theta + 2*pi*k) are different,
 * even though they correspond to the same rotation.
 * And unlike old libmol, we don't do normalization of the rotation angle to [0; 2pi] range inside this function.
 *
 * \param dr_over_dv[out] Pointer to the array of three \ref mol_matrix3 structures for storing the result.
 * \param v[in] Exponential coordinates.
 */
void smp_rmatr_derive_wrt_exp(struct mol_matrix3 *dr_over_dv, const struct mol_vector3 v);

/**
 * Build quaternion from exponential rotation parameters.
 * \param v[in] Exponential coordinates.
 * \return Rotation quaternion.
 */
struct mol_quaternion smp_quaternion_from_exp(const struct mol_vector3 v);

/**
 * Build quaternion from exponential rotation parameters.
 * \param q[in] Rotation quaternion.
 * \return Exponential coordinates.
 */
struct mol_vector3 smp_quaternion_to_exp(const struct mol_quaternion q);

#endif // _SMP_TRANSFORM_EXP_H_
