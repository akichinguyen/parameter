# CMake generated Testfile for 
# Source directory: /opt/conda/conda-bld/rdkit_1565675129650/work/Projects
# Build directory: /opt/conda/conda-bld/rdkit_1565675129650/work/Projects
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(pythonTestDbCLI "/home/thu/Downloads/ligtbm/backend/venv/bin/python" "/opt/conda/conda-bld/rdkit_1565675129650/work/Projects/test_list.py" "--testDir" "/opt/conda/conda-bld/rdkit_1565675129650/work/Projects")
set_tests_properties(pythonTestDbCLI PROPERTIES  _BACKTRACE_TRIPLES "/opt/conda/conda-bld/rdkit_1565675129650/work/Code/cmake/Modules/RDKitUtils.cmake;183;add_test;/opt/conda/conda-bld/rdkit_1565675129650/work/Projects/CMakeLists.txt;1;add_pytest;/opt/conda/conda-bld/rdkit_1565675129650/work/Projects/CMakeLists.txt;0;")
