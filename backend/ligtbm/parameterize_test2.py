#!/usr/bin/env python3

import json
import multiprocessing
import contextlib
import os
import subprocess
import shutil
import logging.config
import tempfile
from pathlib import Path
import re
import click
import numpy as np
import openbabel
import pybel
import prody
import rdkit.Chem
import rdkit.Chem.AllChem
import io


OUT_DIR = 'parameterized'

FILE_DIR = os.path.dirname(os.path.abspath(__file__))
logging.config.fileConfig(os.path.join(FILE_DIR, 'logging.conf'), disable_existing_loggers=False)
LOG_USR = logging.getLogger('user')
LOG_DEV = logging.getLogger('dev')


TLEAP_COMPLEX = '''
source leaprc.protein.ff14SB
source leaprc.gaff
clearPdbResMap
loadamberprep {lig_prepi_path}
loadamberparams {frcmod_path}
mol = loadpdb {cplx_clean_path}
saveamberparm mol {cplx_prmtop_path} {cplx_inpcrd_path}
quit
'''
TLEAP_COMPLEX_REC = '''
source leaprc.protein.ff14SB
source leaprc.gaff
clearPdbResMap
mol = loadpdb {cplx_clean_path}
saveamberparm mol {cplx_prmtop_path} {cplx_inpcrd_path}
quit
'''

#Not sure if makes sense to have this set up here or not
PAR_MERGE = '''
{parm_dat_path}
{frcmod_ff_path}
{frcmod_lig_path}
'''


@contextlib.contextmanager
def cwd(path):
    oldpwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(oldpwd)


def _run_amberbin(bin_name, args, options):
    BIN = Path(options['CONDA_ENV']) / 'bin' / bin_name
    assert BIN.exists(), "{} does not exist".format(BIN)
    args = [BIN] + list(args)
    subprocess.check_call(args)


def _copy_amberparm(parm_name, dest_dir, options):
    PARM = Path(options['CONDA_ENV']) / 'dat' / 'leap' / 'parm' / parm_name
    assert PARM.exists(), "{} does not exist".format(PARM)
    shutil.copy(PARM, dest_dir)


def _run_tleap_lig(template_str, output_prefix, options):
    tleap_cmd = template_str.format(
        mol2_path='lig.am1bcc.mol2',
        resn='LIG',
        lig_prepi_path='lig.prepi',
        frcmod_path='lig.frcmod',
        lib_path='lig.lib',
        lig_prmtop_path='lig.prmtop',
        lig_rst7_path='lig.rst7',
        rec_pdb_path='rec.pdb',
        cplx_pdb_path='complex_lig.naive.pdb',
        cplx_clean_path='complex_clean_lig.pdb',
        cplx_prmtop_path='complex_lig.prmtop',
        cplx_inpcrd_path='complex_lig.inpcrd',
        cplx_rst7_path=output_prefix+'.complex_lig.rst7',
        cplx_pdb_param_path=output_prefix+'.complex_lig.pdb'
    )
    with tempfile.NamedTemporaryFile(suffix='.leap', mode='w') as fp:
        fp.write(tleap_cmd)
        fp.flush()
        args = [
            '-f', fp.name,
            '-s'
        ]
        _run_amberbin('tleap', args, options)

def _run_tleap_rec(template_str, output_prefix, options):
    tleap_cmd = template_str.format(
        rec_pdb_path='rec.pdb',
        cplx_pdb_path='complex_rec.naive.pdb',
        cplx_clean_path='complex_clean_rec.pdb',
        cplx_prmtop_path='complex_rec.prmtop',
        cplx_inpcrd_path='complex_rec.inpcrd',
        cplx_rst7_path=output_prefix+'.complex_rec.rst7',
        cplx_pdb_param_path=output_prefix+'.complex_rec.pdb'
    )
    with tempfile.NamedTemporaryFile(suffix='.leap', mode='w') as fp:
        fp.write(tleap_cmd)
        fp.flush()
        args = [
            '-f', fp.name,
            '-s'
        ]
        _run_amberbin('tleap', args, options)

def _run_tleap(template_str, output_prefix, options):
    tleap_cmd = template_str.format(
        mol2_path='lig.am1bcc.mol2',
        resn='LIG',
        lig_prepi_path='lig.prepi',
        frcmod_path='lig.frcmod',
        lib_path='lig.lib',
        lig_prmtop_path='lig.prmtop',
        lig_rst7_path='lig.rst7',
        rec_pdb_path='rec.pdb',
        cplx_pdb_path='complex.naive.pdb',
        cplx_clean_path='complex_clean.pdb',
        cplx_prmtop_path='complex.prmtop',
        cplx_inpcrd_path='complex.inpcrd',
        cplx_rst7_path=output_prefix+'.complex.rst7',
        cplx_pdb_param_path=output_prefix+'.complex.pdb'
    )
    with tempfile.NamedTemporaryFile(suffix='.leap', mode='w') as fp:
        fp.write(tleap_cmd)
        fp.flush()
        args = [
            '-f', fp.name,
            '-s'
        ]
        _run_amberbin('tleap', args, options)


def _update_prmrtf(prm_file, base_rtf, mol_rtf, output_prm, output_rtf):

    # Read in new prm file
    with open(prm_file, 'r') as f_prm:
        data_prm = f_prm.read()
        re.split('ATOMS|BONDS', data_prm)
        prm = re.split('ATOMS|BONDS', data_prm)

    prm_head = prm[0]
    prm_atoms = prm[1]
    prm_rest = prm[2]

    #BOND+ will need to print out with 'BONDS\n' first
    #not pretty but gets job done (hopefully)

    # Read in the base rtf file
    # Remove MASS entries and END statement
    with open(base_rtf, 'r') as f_rtf:
        data_rtf = f_rtf.readlines()

    check_lines = ('MASS', '!MASS', 'END')
    cleaned_rtf = [x for x in data_rtf if not x.startswith(check_lines)]

    # Find index of first 'DECL' entry and use to split file
    matches = [x for x, e in enumerate(cleaned_rtf) if e.startswith('DECL')]
    rtf_part1 = cleaned_rtf[:matches[0]]
    rtf_part2 = cleaned_rtf[matches[0]:]

    # Read in the mol rtf file
    with open(mol_rtf, 'r') as f_mol:
        data_mol = f_mol.readlines()

    # Write out new prm and rtf files
    with open(output_prm, 'w') as f:
        f.write('%s' % prm_head)
        f.write('BONDS')
        f.write('%s' % prm_rest)

    # Write out new prm and rtf files
    with open(output_rtf, 'w') as f:
        for item in rtf_part1:
            f.write('%s' % item)
        f.write('%s' %prm_atoms)
        for item in rtf_part2:
            f.write('%s' % item)
        for item in data_mol:
            f.write('%s' % item)
        f.write('END\n')

def _update_prmrtf_nomol(prm_file, base_rtf, output_prm, output_rtf):

    # Read in new prm file
    with open(prm_file, 'r') as f_prm:
        data_prm = f_prm.read()
        re.split('ATOMS|BONDS', data_prm)
        prm = re.split('ATOMS|BONDS', data_prm)

    prm_head = prm[0]
    prm_atoms = prm[1]
    prm_rest = prm[2]

    #BOND+ will need to print out with 'BONDS\n' first
    #not pretty but gets job done (hopefully)

    # Read in the base rtf file
    # Remove MASS entries and END statement
    with open(base_rtf, 'r') as f_rtf:
        data_rtf = f_rtf.readlines()

    check_lines = ('MASS', '!MASS', 'END')
    cleaned_rtf = [x for x in data_rtf if not x.startswith(check_lines)]

    # Find index of first 'DECL' entry and use to split file
    matches = [x for x, e in enumerate(cleaned_rtf) if e.startswith('DECL')]
    rtf_part1 = cleaned_rtf[:matches[0]]
    rtf_part2 = cleaned_rtf[matches[0]:]

    # Write out new prm and rtf files
    with open(output_prm, 'w') as f:
        f.write('%s' % prm_head)
        f.write('BONDS')
        f.write('%s' % prm_rest)

    # Write out new prm and rtf files
    with open(output_rtf, 'w') as f:
        for item in rtf_part1:
            f.write('%s' % item)
        f.write('%s' %prm_atoms)
        for item in rtf_part2:
            f.write('%s' % item)
        f.write('END\n')


def _run_antechamber(input_file, output_file, options, *args):
    fi = input_file.split('.')[-1]
    fo = output_file.split('.')[-1]
    cmd_args = [
        '-i', input_file,
        '-fi', fi,
        '-o', output_file,
        '-fo', fo
    ] + list(args)
    _run_amberbin('antechamber', cmd_args, options)


def _run_parmchk2(input_file, output_frcmod, options, *args):
    # update input so more general?
    f = input_file.split('.')[-1]
    cmd_args = [
         '-i', input_file,
         '-f', f,
         '-o', output_frcmod
    ] + list(args)
    _run_amberbin('parmchk2', cmd_args, options)


def _run_pdb4amber(input_pdb, output_pdb, options, *args):
    cmd_args = [
         '-i', input_pdb,
         '-o', output_pdb
    ] + list(args)
    _run_amberbin('pdb4amber', cmd_args, options)


def _run_amb2chm_psfcrd(input_prmtop, input_inpcrd, output_prefix, options):
    args = [
         '-p', input_prmtop,
         '-c', input_inpcrd,
         '-f', output_prefix+'.psf',
         '-d', output_prefix+'.crd',
         '-b', output_prefix+'.crd.pdb'
    ]
    _run_amberbin('amb2chm_psf_crd.py', args, options)


def _run_amb2chm_par(merge_str, frcmod_lig, options):
    parmerge_cmd = merge_str.format(
        parm_dat_path='parm10_noCuFe.dat',
        frcmod_ff_path='frcmod.ff14SB',
        frcmod_lig_path=frcmod_lig
    )
    with tempfile.NamedTemporaryFile(suffix='.par', mode='w') as fp:
        fp.write(parmerge_cmd.strip())
        fp.flush()
        args = [
            '-i', fp.name,
            '-o', 'parm.prm'
        ]
        _run_amberbin('amb2chm_par.py', args, options)


def _run_mol2rtf(input_mol, output_rtf, options):
    args = [
         '-i', input_mol,
         '-o', output_rtf,
         '-r', 'LIG',
         '-n', 'LIG'
    ]
    _run_amberbin('mol2rtf.py', args, options)

def run_antechamber_lig(input_mol, output_prefix, options):
    input_mol_abs = os.path.abspath(input_mol)
    output_prefix_abs = os.path.abspath(output_prefix)
    try:
        shutil.rmtree(output_prefix_abs, ignore_errors=True)
    except OSError:
        pass
    os.mkdir(output_prefix_abs)
    with cwd(output_prefix_abs):
        mol = next(pybel.readfile('mol', input_mol_abs))
        mol.addh()
        mol.localopt('mmff94', steps=500)
        optimized_mol = 'lig.localopt.sdf'
        mol.write('sdf', optimized_mol)
        lig_orig = 'lig_orig.pdb'
        _run_antechamber(optimized_mol, lig_orig, options, '-c', 'gas',
                         '-at', 'sybyl', '-rn', 'LIG', '-dr', 'yes', '-pf', 'yes')

        lig_clean = 'lig_clean.pdb'
        _run_pdb4amber(lig_orig, lig_clean, options)
        lig_prepi = 'lig.prepi'
        _run_antechamber(lig_clean, lig_prepi, options, '-c', 'gas',
                         '-at', 'gaff', '-rn', 'LIG', '-dr', 'yes', '-pf', 'yes')
        lig_frcmod = 'lig.frcmod'
        _run_parmchk2(lig_prepi, lig_frcmod, options)
        complex_merge = 'complex_lig.pdb'
        with open(complex_merge, 'wb') as destination:
            shutil.copyfileobj(open(lig_clean, 'rb'), destination)
        subprocess.call(['sed', '-i', '/OXT/d', complex_merge])
        complex_clean = 'complex_clean_lig.pdb'
        _run_pdb4amber(complex_merge, complex_clean, options)
        _run_tleap_lig(TLEAP_COMPLEX, output_prefix_abs, options)
        return {
            'prepi' : output_prefix_abs + '/' + lig_prepi,
            'prmtop': output_prefix_abs + '/complex_lig.prmtop',
            'inpcrd': output_prefix_abs + '/complex_lig.inpcrd'
        }


def run_antechamber_rec(input_receptor_pdb, output_prefix, options):
    input_rec_abs = os.path.abspath(input_receptor_pdb)
    output_prefix_abs = os.path.abspath(output_prefix)

    try:
        shutil.rmtree(output_prefix_abs, ignore_errors=True)
    except OSError:
        pass
    os.mkdir(output_prefix_abs)
    with cwd(output_prefix_abs):
        mutation_str = list_mutations(input_rec_abs)
        rec_clean = 'rec_clean.pdb'
        if mutation_str:
            _run_pdb4amber(input_rec_abs, rec_clean, options, '-m', mutation_str)
        else:
            _run_pdb4amber(input_rec_abs, rec_clean, options)
        subprocess.call(['sed', '-i', '/HETATM/d', rec_clean])
        complex_merge = 'complex_rec.pdb'
        with open(complex_merge, 'wb') as destination:
            shutil.copyfileobj(open(rec_clean, 'rb'), destination)
        subprocess.call(['sed', '-i', '/OXT/d', complex_merge])
        complex_clean = 'complex_clean_rec.pdb'
        _run_pdb4amber(complex_merge, complex_clean, options)
        _run_tleap_rec(TLEAP_COMPLEX_REC, output_prefix_abs, options)
        return {
            'prmtop': output_prefix_abs + '/complex_rec.prmtop',
            'inpcrd': output_prefix_abs + '/complex_rec.inpcrd'
        }

def run_antechamber(input_mol, input_receptor_pdb, output_prefix, options):
    input_mol_abs = os.path.abspath(input_mol)
    input_rec_abs = os.path.abspath(input_receptor_pdb)
    output_prefix_abs = os.path.abspath(output_prefix)
    try:
        shutil.rmtree(output_prefix_abs, ignore_errors=True)
    except OSError:
        pass
    os.mkdir(output_prefix_abs)

    with cwd(output_prefix_abs):
        # Add hydrogens and optimize locally
        mol = next(pybel.readfile('mol', input_mol_abs))
        mol.addh()
        mol.localopt('mmff94', steps=500)
        optimized_mol = 'lig.localopt.sdf'
        mol.write('sdf', optimized_mol)

        # Assign Gasteiger charges and calculate net charge
        lig_orig = 'lig_orig.pdb'
        _run_antechamber(optimized_mol, lig_orig, options, '-c', 'gas',
                         '-at', 'sybyl', '-rn', 'LIG', '-dr', 'yes', '-pf', 'yes')

        lig_clean = 'lig_clean.pdb'
        _run_pdb4amber(lig_orig, lig_clean, options)

        # Transform Gasteiger charges to AM1-BCC charges
        lig_prepi = 'lig.prepi'
        _run_antechamber(lig_clean, lig_prepi, options, '-c', 'gas',
                         '-at', 'gaff', '-rn', 'LIG', '-dr', 'yes', '-pf', 'yes')
        lig_frcmod = 'lig.frcmod'

        # Check params and generate .frcmod file
        _run_parmchk2(lig_prepi, lig_frcmod, options)

        # Identify modified residues in rec.pdb
        # pdb4amber will replace residue name of backbone atoms
        # but keeps original residue atoms as HETATM entries
        mutation_str = list_mutations(input_rec_abs)
        rec_clean = 'rec_clean.pdb'
        if mutation_str:
            _run_pdb4amber(input_rec_abs, rec_clean, options, '-m', mutation_str)
        else:
            _run_pdb4amber(input_rec_abs, rec_clean, options)
        # Remove any HETATM records in receptor
        subprocess.call(['sed', '-i', '/HETATM/d', rec_clean])

        # Combine protein and ligand pdb files and clean format
        # Use lig_clean or lig_fin here?
        complex_merge = 'complex.pdb'
        with open(complex_merge, 'wb') as destination:
            shutil.copyfileobj(open(rec_clean, 'rb'), destination)
            shutil.copyfileobj(open(lig_clean, 'rb'), destination)

        # Remove any OXT records in complex
        subprocess.call(['sed', '-i', '/OXT/d', complex_merge])

        # Is cleaning needed after merge, now that pdb4amber already run on rec/lig
        complex_clean = 'complex_clean.pdb'
        _run_pdb4amber(complex_merge, complex_clean, options)

        # Generate topology and coordinate files for complex
        # No terminal patches
        _run_tleap(TLEAP_COMPLEX, output_prefix_abs, options)

        return {
            'prepi' : output_prefix_abs + '/' + lig_prepi,
            'prmtop': output_prefix_abs + '/complex.prmtop',
            'inpcrd': output_prefix_abs + '/complex.inpcrd'
        }

def convert_amb2chm_lig(input_files, output_prefix, options):
    output_prefix_abs = os.path.abspath(output_prefix)
    with cwd(output_prefix_abs):
        amb_prmtop = input_files['prmtop']
        amb_inpcrd = input_files['inpcrd']
        lig_prepi = input_files['prepi']
        amb_file = 'complex_lig'
        _run_amb2chm_psfcrd(amb_prmtop, amb_inpcrd, amb_file, options)
        lig_mol2 = 'lig.mol2'
        _run_antechamber(lig_prepi, lig_mol2, options, '-pf', 'y')
        lig_finfrcmod = 'lig_fin.frcmod'
        _run_parmchk2(lig_mol2, lig_finfrcmod, options, '-a', 'Y')
        datfile = 'parm10_noCuFe.dat'
        frcmod14sb = 'frcmod.ff14SB'
        _copy_amberparm(frcmod14sb, output_prefix_abs, options)
        _copy_amberparm(datfile, output_prefix_abs, options)

        lig_rtf = 'lig.rtf'
        _run_mol2rtf(lig_mol2, lig_rtf, options)
        _run_amb2chm_par(PAR_MERGE, lig_finfrcmod, options)
        shutil.copy(Path(options['CHARMM_RTF']), output_prefix_abs)
        lig_out = 'lig_out'
        orig_rtf = os.path.basename(options['CHARMM_RTF'])
        output_prm = '%s.prm' % lig_out.lower()
        output_rtf = '%s.rtf' % lig_out.lower()
        _update_prmrtf('parm.prm', orig_rtf, lig_rtf, output_prm, output_rtf)
        subprocess.call(['sed', '-i', 's/HETATM/ATOM  /g', 'complex_lig.crd.pdb'])
        updated_complex = 'complex_lig.crd.renum.pdb'
        update_resnumchain('complex_lig.pdb', 'complex_lig.crd.pdb', updated_complex)
        subprocess.call(['sblu', 'pdb', 'prep', updated_complex, '--no-clean',
                         '--no-minimize',  '--prm', output_prm, '--rtf', output_rtf])
        return {
            'forcefield_prm': output_prefix + '/' + output_prm,
            'forcefield_rtf': output_prefix + '/' + output_rtf,
            'complex_psf': output_prefix + '/prepared_lig.psf',
            'complex_pdb': output_prefix + '/prepared_lig.pdb'
        }


def convert_amb2chm_rec(input_files, output_prefix, options):
    output_prefix_abs = os.path.abspath(output_prefix)
    with cwd(output_prefix_abs):
        amb_prmtop = input_files['prmtop']
        amb_inpcrd = input_files['inpcrd']
        amb_file = 'complex_rec'
        _run_amb2chm_psfcrd(amb_prmtop, amb_inpcrd, amb_file, options)
        datfile = 'parm10_noCuFe.dat'
        frcmod14sb = 'frcmod.ff14SB'
        _copy_amberparm(frcmod14sb, output_prefix_abs, options)
        _copy_amberparm(datfile, output_prefix_abs, options)
        shutil.copy(Path(options['CHARMM_RTF']), output_prefix_abs)
        orig_rtf = os.path.basename(options['CHARMM_RTF'])
        rec_out = 'rec_out'
        output_prm = '%s.prm' % rec_out.lower()
        output_rtf = '%s.rtf' % rec_out.lower()
        _update_prmrtf_nomol('parm.prm', orig_rtf, output_prm, output_rtf)
        subprocess.call(['sed', '-i', 's/HETATM/ATOM  /g', 'complex_rec.crd.pdb'])
        updated_complex = 'complex_rec.crd.renum.pdb'
        update_resnumchain('complex_rec.pdb', 'complex_rec.crd.pdb', updated_complex)
        subprocess.call(['sblu', 'pdb', 'prep', updated_complex, '--no-clean',
                         '--no-minimize',  '--prm', output_prm, '--rtf', output_rtf])
        return {
            'forcefield_prm': output_prefix + '/' + output_prm,
            'forcefield_rtf': output_prefix + '/' + output_rtf,
            'complex_psf': output_prefix + '/prepared_rec.psf',
            'complex_pdb': output_prefix + '/prepared_rec.pdb'
        }

def convert_amb2chm(input_files, output_prefix, options):
    output_prefix_abs = os.path.abspath(output_prefix)
    with cwd(output_prefix_abs):
        amb_prmtop = input_files['prmtop']
        amb_inpcrd = input_files['inpcrd']
        lig_prepi = input_files['prepi']

        # Generate psf (new style) and crd file for complex
        amb_file = 'complex'
        _run_amb2chm_psfcrd(amb_prmtop, amb_inpcrd, amb_file, options)

        # Generate a frcmod file for ligand with complete force field parameters
        lig_mol2 = 'lig.mol2'
        _run_antechamber(lig_prepi, lig_mol2, options, '-pf', 'y')
        lig_finfrcmod = 'lig_fin.frcmod'
        _run_parmchk2(lig_mol2, lig_finfrcmod, options, '-a', 'Y')

        # Copy prm files (parm10_noCuFe.data and frcmod.ff14SB)
        # Planted parm10_noCuFe.dat in local conda dat directory
        # Normal parm10.dat will fail due to missing vdw for Cu and Fe
        datfile = 'parm10_noCuFe.dat'
        frcmod14sb = 'frcmod.ff14SB'
        _copy_amberparm(frcmod14sb, output_prefix_abs, options)
        _copy_amberparm(datfile, output_prefix_abs, options)

        lig_rtf = 'lig.rtf'
        _run_mol2rtf(lig_mol2, lig_rtf, options)

        # Call amb2chm_par.py to merge parm and frcmod files
        # TODO: merge the parm files without copying  to directory...
        _run_amb2chm_par(PAR_MERGE, lig_finfrcmod, options)

        # Copy pre-converted RTF (with no TIP3 lines)
        # Eventually could add to mol-prms repository
        shutil.copy(Path(options['CHARMM_RTF']), output_prefix_abs)
        orig_rtf = os.path.basename(options['CHARMM_RTF'])
        output_prm = '%s.prm' % os.path.basename(output_prefix_abs).lower()
        output_rtf = '%s.rtf' % os.path.basename(output_prefix_abs).lower()

        # hmmm, should set parm.prm var somewhere
        _update_prmrtf('parm.prm', orig_rtf, lig_rtf, output_prm, output_rtf)

        # Replace HETATM records with ATOM records
        subprocess.call(['sed', '-i', 's/HETATM/ATOM  /g', 'complex.crd.pdb'])

        # Rename chains and renumber residues of complex.crd.pdb to match
        # original numbering from complex.pdb
        updated_complex = 'complex.crd.renum.pdb'
        update_resnumchain('complex.pdb', 'complex.crd.pdb', updated_complex)

        # Use sblu preparation step
        subprocess.call(['sblu', 'pdb', 'prep', updated_complex, '--no-clean',
                         '--no-minimize',  '--prm', output_prm, '--rtf', output_rtf])

        return {
            'forcefield_prm': output_prefix + '/' + output_prm,
            'forcefield_rtf': output_prefix + '/' + output_rtf,
            'complex_psf': output_prefix + '/prepared.psf',
            'complex_pdb': output_prefix + '/prepared.pdb'
        }

def list_mutations(rec_pdb):
    # Grab list of nonstandard residues
    pdb = prody.parsePDB(rec_pdb)
    nonstd = pdb.select('nonstdaa')
    if nonstd:
        resnums_uniq = nonstd.select('calpha').getResnums()
        resnames_uniq = nonstd.select('calpha').getResnames()

        # Acceptable substitutions
        substitutions = {'ASX':'ASP', 'GLX':'GLU', 'CSO':'CYS', 'HSD':'HID',
                         'HSE':'HIE', 'HSP':'HIP', 'MSE':'MET', 'SEC':'CYS',
                         'SEP':'SER', 'TPO':'THR', 'PTR':'TYR', 'XLE':'LEU'}
        # Residues accepted by leap, but considered nonstandard by prody
        accepted_nonsta = ['CYX', 'HIP', 'HID', 'HIE']
        # Ambiguous residues (i.e. ASX refers to ASP or ASN)
        ambig_nonsta = ['ASX', 'GLX', 'XLE']

        mutations = []

        # Loop through and identify residues to mutate
        for resn,resi in zip(resnums_uniq, resnames_uniq):
            if resi in substitutions.keys():
                if resi in ambig_nonsta:
                    LOG_USR.info('Ambiguous residue %s in PDB file. Mutating to %s', resi, substitutions[resi])
                mutations.append('%s-%s'%(resn, substitutions[resi]))
            elif resi in accepted_nonsta:
                LOG_USR.info('%s accepted by AMBER', resi)
            else:
                LOG_USR.warning('No parameters for %s residue', resi)
                LOG_DEV.warning('No parameters for %s residue', resi)

        mutation_str = ','.join(mutations)
        return mutation_str

def update_resnumchain(orig_complex, new_complex, updated_complex):
    # Load in the pdb files
    oldpdb = prody.parsePDB(orig_complex)
    newpdb = prody.parsePDB(new_complex)

    resnames_ca = newpdb.select('calpha').getResnames()
    resnums_ca = newpdb.select('calpha').getResnums()
    resnums_uniq = set(newpdb.getResnums())

    # List to contain atom count per residue from new_complex
    atom_cts = []
    for resn in resnums_uniq:
        selection = "resnum %s"%resn
        atom_cts.append(newpdb.select(selection).numAtoms())

    # Adjust residue names and residue numbers for LIG
    resnames_ca = np.append(resnames_ca, 'LIG')
    resnums_ca = np.append(resnums_ca, resnums_ca[-1]+1)

    # Residue names, numbers, atom counts from new_complex
    merged_new = tuple(zip(resnames_ca, resnums_ca, atom_cts))

    # Get information from original complex
    oldpdb_names = oldpdb.select('calpha').getResnames()
    oldpdb_nums = oldpdb.select('calpha').getResnums()
    oldpdb_chs = oldpdb.select('calpha').getChids()

    # Adjust residue names, numbers, and chain for LIG
    oldpdb_names = np.append(oldpdb_names, 'LIG')
    oldpdb_nums = np.append(oldpdb_nums, 1)
    oldpdb_chs = np.append(oldpdb_chs, '')

    # Check that number of residues in old vs new pdb file is consistent
    assert len(resnames_ca) == len(oldpdb_names)

    # Residue names, numbers, atom counts from original complex
    merged_old = tuple(zip(oldpdb_names, oldpdb_nums, oldpdb_chs))

    # Lists for updated residue names and chain identifiers
    update_resnums = []
    update_chains = []

    for resi, info in enumerate(merged_new):
        orig_resn = merged_old[resi][1]
        orig_chain = merged_old[resi][2]
        atom_ct = info[2]
        update_resnums.append([orig_resn]*atom_ct)
        update_chains.append([orig_chain]*atom_ct)

    # Switch to numpy array
    renum_resi = np.concatenate([np.array(i) for i in update_resnums])
    rename_chain = np.concatenate([np.array(i) for i in update_chains])

    # Set residue numbers and chain identiers for new complex pdb
    newpdb.setResnums(renum_resi)
    newpdb.setChids(rename_chain)
    prody.writePDB(updated_complex, newpdb)


def read_mol_with_pybel(mol_filename):
    mol = next(pybel.readfile('mol', mol_filename))
    assert mol.dim == 3  # We managed to get coordinates from mol file
    return mol


def read_residue_from_pdb_with_prody(pdb_filename, target_resn='LIG'):
    ag = prody.parsePDB(pdb_filename)
    ag_lig = ag.select('resname {}'.format(target_resn))
    return ag_lig.copy()


def read_residue_from_pdb_with_rdkit(pdb_filename, target_resn='LIG'):
    pdb_data = prody.parsePDB(pdb_filename).select('resname {}'.format(target_resn))
    with io.StringIO() as fp:
        prody.writePDBStream(fp, pdb_data)
        pdb_string = fp.getvalue()
    return rdkit.Chem.MolFromPDBBlock(pdb_string)


def crd_eq(a, b, distance_max):
    a = np.array(a)
    b = np.array(b)
    d = np.sum((a - b) ** 2) ** 0.5
    return d < distance_max


def check_mapping_by_elements(mapping, elements_from, elements_to):
    # Sanity-check of the mapping
    if elements_from is not None and elements_to is not None:
        for index_from, index_to in mapping.items():
            if elements_from[index_from].upper() != elements_to[index_to].upper():
                raise ValueError('Mapped atom {} ({}) to atom {} ({})'.format(
                    index_from, elements_from[index_from], index_to, elements_to[index_to]
                ))


def map_ligands_by_coords(coords_from, coords_to):
    max_distance = 0.01  # Angstrom; should be within file format precision
    mapping = {}  # old atom index to new atom index
    # Atom names are total mess. We just compare atoms by their coordinates
    for index_from, crd_from in enumerate(coords_from):
        flags = [crd_eq(crd_from, i, max_distance) for i in coords_to]
        indexes_to_good = [idx for idx, f in enumerate(flags) if f]
        if len(indexes_to_good) == 1:
            mapping[index_from] = indexes_to_good[0]
        elif len(indexes_to_good) > 1:
            raise ValueError('Multiple mappings for ({}, {}, {})'.format(*crd_from))
        else:
            raise ValueError('No mapping for ({}, {}, {})'.format(*crd_from))

    return mapping


def _get_pybel_element(atom):
    etab = openbabel.OBElementTable()
    return etab.GetSymbol(atom.atomicnum)


def _get_coordinates_from_rdkit_mol(mol, atom_indices):
    conformer = mol.GetConformer()
    coords = [list(conformer.GetAtomPosition(i)) for i in atom_indices]
    coords_arr = np.array(coords)
    assert coords_arr.shape == (conformer.GetNumAtoms(), 3)
    return coords_arr


def _rmsd_of_rdkit_match(rdkit_mol1, rdkit_mol2, match):
    # match: The first index is for the atom in mol1 that matches the first atom in mol2
    idx1 = match
    idx2 = list(range(len(match)))
    coords1 = _get_coordinates_from_rdkit_mol(rdkit_mol1, idx1)
    coords2 = _get_coordinates_from_rdkit_mol(rdkit_mol2, idx2)
    return prody.calcRMSD(coords1, coords2)


def _clear_molecular_details(rdkit_mol):
    # Here, we remove hydrogens and reset bond chirality.
    # The later is needed because if we load structures from PDB, we don't know which of the bonds are double, which
    # are single, etc. The order of every bond is "unspecified"
    # However, when we load structure from MOL file, it has the order of the bonds explicitly listed.
    # And if we try to use GetSubstructureMatches on these two objects, it will fail, because it takes bond order
    # into account.
    # We could have solved it by using AssignBondOrdersFromTemplate(from_mol, from_pdb). But in
    # ambiguous cases it can assign double vs. single bonds not the same way they are specified in MOL file,
    # potentially breaking chirality. For example, if the MOL file has the 3D structure as depicted on the *right*,
    # the PDB file might still end up with bonds assigned as depicted on the *left*.
    #
    #    O         O             O         O
    #     \       /               \      //
    #      C --- C                 C --- C
    #     //      \\              //      \
    #    O          O            O         O
    #
    # And since we later anyway do exhaustive substructure matching, we better just remove the bond order in all
    # molecules to simplify our life. They are the same molecule anyway, we just need to find best mapping for
    # their atoms to make 3D restraints.
    mol_noh = rdkit.Chem.RemoveHs(rdkit_mol)
    params = rdkit.Chem.AllChem.AdjustQueryParameters()
    params.makeBondsGeneric = True
    return rdkit.Chem.AllChem.AdjustQueryProperties(mol_noh, params)


def map_structure_pdb_to_mol(mol_file, pdb_file):
    max_rmsd_allowed = 4  # Angstroms; max. allowed RMSD between PDB and MOL files; chosen arbitrary
    max_distance_allowed = 4  # Angstroms; same as above, but for individual pairs of matching atoms

    # Get heavy atoms only
    mol_data = _clear_molecular_details(rdkit.Chem.MolFromMolFile(mol_file))
    pdb_data = _clear_molecular_details(read_residue_from_pdb_with_rdkit(pdb_file))
    # Make substructure match and select best one
    matches = mol_data.GetSubstructMatches(pdb_data, useChirality=False, uniquify=False)
    best_match = sorted(matches, key=lambda match: _rmsd_of_rdkit_match(mol_data, pdb_data, match))[0]
    # Sanity check
    if _rmsd_of_rdkit_match(mol_data, pdb_data, best_match) > max_rmsd_allowed:
        raise ValueError('Even the best match of MOL to PDB substructure is bad')
    best_mapping = {v: i for i, v in enumerate(best_match)}
    # Extract coordinates
    coords_mol_heavy = _get_coordinates_from_rdkit_mol(mol_data, best_mapping.keys())
    coords_pdb_heavy = _get_coordinates_from_rdkit_mol(pdb_data, best_mapping.values())
    # Very basic sanity check
    for c1, c2 in zip(coords_mol_heavy, coords_pdb_heavy):
        if not crd_eq(c1, c2, max_distance_allowed):
            raise ValueError('Big discrepancy in individual coordinates of matched atoms: {} in MOL, {} in PDB'.format(
                str(c1), str(c2)
            ))
    # Now, best-match maps heavy atom in order interpreted by RDKit.
    #   But original mapping is in terms of all-atom structure, and RDKit might have messed up atom ordering.
    #   So we read all-atom files and map atoms by coordinates.
    pdb_data_allatom = read_residue_from_pdb_with_prody(pdb_file, 'LIG')
    mol_data_allatom = read_mol_with_pybel(mol_file)
    coords_mol_allatom = [at.coords for at in mol_data_allatom]
    coords_pdb_allatom = pdb_data_allatom.getCoords()
    # Since heavy (RDKit-made) coordinates are now in matching order, we just
    mapping_mol_heavy_to_all = map_ligands_by_coords(coords_mol_heavy, coords_mol_allatom)
    mapping_pdb_heavy_to_all = map_ligands_by_coords(coords_pdb_heavy, coords_pdb_allatom)
    # And now we combine these three mappings
    mapping_pdb_to_mol_all = {
        mapping_pdb_heavy_to_all[idx_from]: mapping_mol_heavy_to_all[idx_from]
        for idx_from in range(len(mapping_mol_heavy_to_all))
    }
    # One more sanity check
    for idx_pdb, idx_mol in mapping_pdb_to_mol_all.items():
        if not crd_eq(coords_mol_allatom[idx_mol], coords_pdb_allatom[idx_pdb], max_distance_allowed):
            raise ValueError('Extremely strange discrepancy after re-mapping atom numbers')
    # And the last one
    mol_elements = [_get_pybel_element(at) for at in mol_data_allatom]
    pdb_elements = pdb_data_allatom.getElements()
    for idx_pdb, idx_mol in mapping_pdb_to_mol_all.items():
        if mol_elements[idx_mol].upper() != pdb_elements[idx_pdb].upper():
            raise ValueError('Mismatch of elements: {} in MOL, {} in PDB'.format(
                mol_elements[idx_mol], pdb_elements[idx_pdb]
            ))
    return mapping_pdb_to_mol_all


def get_template_coordinates_for_starting_mol(ref_parts):
    """
    Return dictionary mapping zero-based indices in starting mol file to corresponding 3D coordinates of template atom
    """
    mapping = {}
    for ref_part in ref_parts:
        ref_mol_name = ref_part['ref_lig']
        if ref_mol_name.endswith('.mol'):
            ref_mol = read_mol_with_pybel(ref_part['ref_lig'])
            # pybel.Molecule is not subscriptable as is, so we copy coords to array.
            # Warning: doing `list(ref_mol)` and similar things subtly corrupts the data.
            ref_mol_coords = [at.coords for at in ref_mol]
        elif ref_mol_name.endswith('.pdb'):
            ref_mol = prody.parsePDB(ref_mol_name)
            ref_mol_coords = ref_mol.getCoords()
            for i in ref_mol_coords:
                assert len(i) == 3
        for atompair in ref_part['mapping']:
            ref_atom_id = atompair['ref_atom_id']
            target_atom_id = atompair['target_atom_id']
            ref_coords = ref_mol_coords[ref_atom_id]
            mapping[target_atom_id] = ref_coords
    return mapping


# def make_restraints(complex_pdb_in, start_mol_in, ref_parts, rest_pdb_out):
#     ligand_pdb = read_residue_from_pdb_with_prody(complex_pdb_in, 'LIG')
#
#     mapping_pdb_to_mol_indices = map_structure_pdb_to_mol(start_mol_in, complex_pdb_in)
#     mapping_mol_indices_to_reference_coords = get_template_coordinates_for_starting_mol(ref_parts)
#
#     temp_atomgroup = ligand_pdb.copy()
#     atom_has_mapping = [False for _ in temp_atomgroup]
#
#     for pdb_index, atom in enumerate(temp_atomgroup):
#         if pdb_index in mapping_pdb_to_mol_indices.keys():
#             mol_index = mapping_pdb_to_mol_indices[pdb_index]
#             if mol_index in mapping_mol_indices_to_reference_coords.keys():
#                 atom_has_mapping[pdb_index] = True
#                 ref_coords = mapping_mol_indices_to_reference_coords[mol_index]
#                 atom.setCoords(ref_coords)
#
#     temp_atomgroup.setFlags('has_mapping', atom_has_mapping)
#     restrained_atoms = temp_atomgroup.select('has_mapping')
#     assert len(restrained_atoms) == len(mapping_mol_indices_to_reference_coords)
#     prody.writePDB(rest_pdb_out, restrained_atoms)


def do_single_case_rec(rec, options):
    output_prefix = os.path.join(OUT_DIR, "")
    try:
        output_files = run_antechamber_rec(rec, output_prefix, options)
        convert_amb2chm_rec(output_files, output_prefix, options)
    except Exception as exc:
        return None

def do_single_case_lig(lig, options):
    output_prefix = os.path.join(OUT_DIR, "")
    try:
        output_files = run_antechamber_lig(lig, output_prefix, options)
        output_convert = convert_amb2chm_lig(output_files, output_prefix, options)
        map_structure_pdb_to_mol(output_convert['complex_pdb'], lig)
    except Exception as exc:
        return None

def do_single_case(rec, lig, options):
    output_prefix = os.path.join(OUT_DIR, "")
    try:
        output_files = run_antechamber(lig, rec, output_prefix, options)
        output_convert = convert_amb2chm(output_files, output_prefix, options)
        map_structure_pdb_to_mol(output_convert['complex_pdb'], lig)
    except Exception as exc:
        return None

def main(rec, lig, options, flag):
    if(flag == 1):
        do_single_case_rec(rec, options)
    if(flag == 2):
        do_single_case_lig(lig, options)
    if(flag == 3):
        do_single_case(rec, lig, options)


@click.command()
@click.option('-r', type=str)
@click.option('-l', type=str)
@click.argument('options', type=click.File('r'))
def cli(r, l, options):
    options_data = json.load(options)
    if(r!=None and l!=None):
        main(r, l, options_data, 3)
    elif(r!=None and l==None):
        main(r, l, options_data, 1)
    elif(r==None and l!=None):
        main(r, l, options_data, 2)
    else:
        print("Error")

if __name__ == '__main__':
    cli()
