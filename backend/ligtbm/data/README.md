Notice
------

When adding new data, please add its source to this file

Data sources
------------

- The following are most recent CHARMM-like parameters. During D3R, older versions from MCM were used, but they differ only in some really obscure atom types.
  - `charmm_param.prm`: https://bitbucket.org/bu-structure/mol-prms/src/b0c3c6d3992e3fca56e157301bc9d7fe6a837d34/charmm/charmm_param.prm
  - `charmm_param.rtf`: https://bitbucket.org/bu-structure/mol-prms/src/b0c3c6d3992e3fca56e157301bc9d7fe6a837d34/charmm/charmm_param.rtf
  - `parm14sb_notip3.rtf`: https://github.com/openmm/openmmforcefields/tree/master/charmm/toppar/non_charmm/parm14sb_all.rtf
      - Edited copy from openmm github with TIPS3P water model removed.
      - Includes JOAA, JOGA, JOGG, JOAG, JOAP, JOGP patches (originally from pdbamino.rtf) with additional dihedrals and updated atom naming to follow Amber naming.
- Standard PIPER atom parameters.
  - `atoms.0.0.6.prm.ms.3cap+0.5ace.Hr0rec`: https://bitbucket.org/bu-structure/mol-prms/src/b0c3c6d3992e3fca56e157301bc9d7fe6a837d34/atom/atoms.0.0.6.prm.ms.3cap%2B0.5ace.Hr0rec
- Modified AMBER dat files.
  - `parm10_noCuFe.dat`: Edited copy of parm10.dat parameter file (originally from in Ambertools https://anaconda.org/AmberMD/ambertools). Cu and Fe entries have been removed as they are missing atom van de Waals parameters.
- LigandExpo.
  - This version of Ligand Expo Database was generated using Ligand_Expo_correction.ipynb Jupyter Notebook and data available on http://ligand-expo.rcsb.org/ld-download.htm
