#define _POSIX_C_SOURCE 200809L
#include <getopt.h>
#include <jansson.h>
#include <stdbool.h>
#include <mol2/atom_group.h>
#include <mol2/icharmm.h>
#include <mol2/minimize.h>
#include <mol2/pdb.h>
#include <mol2/fitting.h>
#include <mol2/benergy.h>
#include <mol2/nbenergy.h>
#include <mol2/genborn.h>
#include <sampling/energy.h>
#include <sampling/transform.h>
#include <sampling/utils.h>

#define __FILENAME__ "main.c"


void __die(const char *message_format, va_list message_args) __attribute__((noreturn));
void __die(const char *message_format, va_list message_args)
{
	fprintf(stderr, "Error: ");
	vfprintf(stderr, message_format, message_args);
	fprintf(stderr, "\n");
	exit(EXIT_FAILURE);
}


void _die_if(bool is_error, const char *message_format, ...)
{
	va_list message_args;
	va_start (message_args, message_format);
	if (is_error) {
		__die(message_format, message_args);
	}
	va_end(message_args);
}


void _freeze_all_unfreeze_atom_list(struct mol_atom_group *ag, const struct mol_index_list atom_list)
{
	for (size_t i = 0; i < ag->natoms; i++) {
		ag->fixed[i] = true;
	}

	for (size_t i = 0; i < atom_list.size; i++) {
		const size_t j = atom_list.members[i];
		ag->fixed[j] = false;
	}
}


void _unfreeze_atoms_from_list_if_near(
	struct mol_atom_group *ag,
	const struct mol_index_list list_to_unfreeze,
	const double range,
	const struct mol_index_list list_reference)
{
	const double range_sq = range * range;
	for (size_t i = 0; i < list_to_unfreeze.size; i++) {
		const size_t atom_i = list_to_unfreeze.members[i];
		for (size_t j = 0; j < list_reference.size; j++) {
			const size_t atom_j = list_reference.members[j];
			const double dist_sq = MOL_VEC_EUCLIDEAN_DIST_SQ(ag->coords[atom_i], ag->coords[atom_j]);
			const bool is_in_range = (dist_sq < range_sq);
			if (is_in_range)
				ag->fixed[atom_i] = false;
		}
	}
}


static void _get_backbone_in_list(
		struct mol_index_list *list_sidechains,
		const struct mol_atom_group *ag,
		const struct mol_index_list *list_in)
{
	list_sidechains->members = calloc(list_in->size, sizeof(size_t));
	list_sidechains->size = 0;
	for (size_t i = 0; i < list_in->size; i++) {
		const size_t atom_i = list_in->members[i];
		if (mol_is_backbone(ag, atom_i, NULL)) {
			list_sidechains->members[list_sidechains->size] = atom_i;
			list_sidechains->size++;
		}
	}
	// repack
	list_sidechains->members = realloc(list_sidechains->members, list_sidechains->size * sizeof(size_t));
}


#define MAX_ELEM_LENGTH 10
static void _get_heavy(
		struct mol_index_list *list_heavy,
		const struct mol_atom_group *ag)
{
	list_heavy->members = calloc(ag->natoms, sizeof(size_t));
	list_heavy->size = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		char cur_element[MAX_ELEM_LENGTH+1];
		smp_str_trim_and_copy(cur_element, ag->element[i], MAX_ELEM_LENGTH);
		if (strcmp(cur_element, "H") != 0) {  // Not a hydrogen
			list_heavy->members[list_heavy->size++] = i;
		}
	}
	list_heavy->members = realloc(list_heavy->members, list_heavy->size * sizeof(size_t));
}
#undef MAX_ELEM_LENGTH


struct detailed_energy {
	double vdw;
	double vdw_s03;
	double coulomb;
	double coulomb_s03;
	double bonds;
	double angles;
	double impropers;
	double torsions;
	double restraints;
	double density;
	double genborn;
};


struct restraints_params {
	struct mol_index_list atom_indices;
	struct mol_vector3 *coords;
	double k_spring;
};


void _update_restraints_params_from_list(
		struct restraints_params *rest,
		const struct mol_index_list *atom_list,
		const struct mol_atom_group *ag_ref,
		bool append)
{
	if (!append) {
		rest->atom_indices.size = 0;
	}
	const size_t new_size = rest->atom_indices.size + atom_list->size;
	rest->atom_indices.members = realloc(rest->atom_indices.members, new_size * sizeof(size_t));
	rest->coords = realloc(rest->coords, new_size * sizeof(struct mol_vector3));
	for (size_t i = 0; i < atom_list->size; i++) {
		const size_t i_new = rest->atom_indices.size + i;
		const size_t atom_i = atom_list->members[i];
		rest->atom_indices.members[i_new] = atom_i;
		rest->coords[i_new] = ag_ref->coords[atom_i];
	}
	rest->atom_indices.size = new_size;
	// Reorder the array by atom index
	// Naive selection sort, because hopefully we won't be doing it too often
	// Be aware: the original rest->coords might not come from ag_ref, so we can not simply sort rest->atom_indices
	//           and re-create rest->coords from ag_ref.
	for (size_t i = 0; i < rest->atom_indices.size; i++) {
		for (size_t j = i+1; j < rest->atom_indices.size; j++) {
			if (rest->atom_indices.members[i] > rest->atom_indices.members[j]) { // swap two elements
				const size_t tmp_index_i = rest->atom_indices.members[i];
				rest->atom_indices.members[i] = rest->atom_indices.members[j];
				rest->atom_indices.members[j] = tmp_index_i;
				const struct mol_vector3 tmp_coord_i = rest->coords[i];
				rest->coords[i] = rest->coords[j];
				rest->coords[j] = tmp_coord_i;
			}
		}
	}
}


void _remove_from_list(struct mol_index_list *list, const struct mol_index_list *to_remove) {
	static const size_t removal_mark = SIZE_MAX; // Should be greater than any other element in list
	for (size_t i = 0; i < list->size; i++) {
		for (size_t j = 0; j < to_remove->size; j++) {
			if (list->members[i] == to_remove->members[j]) {
				list->members[i] = removal_mark;
			}
		}
	}
	for (size_t i = 0; i < list->size; i++) {
		for (size_t j = i + 1; j < list->size; j++) {
			if (list->members[i] > list->members[j]) { // swap two elements
				const size_t tmp_index_i = list->members[i];
				list->members[i] = list->members[j];
				list->members[j] = tmp_index_i;
			}
		}
	}
	while (list->size > 0 && list->members[list->size-1] == removal_mark)
		list->size--;
	list->members = realloc(list->members, list->size * sizeof(size_t));
}


void _duplicate_list(struct mol_index_list *dst, const struct mol_index_list *src)
{
	mol_list_init(*dst, src->size);
	memcpy(dst->members, src->members, src->size * sizeof(size_t));
}

void _duplicate_restraints_params(struct restraints_params *dst, const struct restraints_params *src)
{
	dst->k_spring = src->k_spring;
	const size_t n_atoms = src->atom_indices.size;
	_duplicate_list(&dst->atom_indices, &src->atom_indices);
	dst->coords = calloc(n_atoms, sizeof(struct mol_vector3));
	memcpy(dst->coords, src->coords, n_atoms * sizeof(struct mol_vector3));
}


struct density_params {
	struct mol_atom_group *ref_ag;
	bool no_grad;
	bool *mask;
	double weight;
	double radius;
};


struct potential_params {
	struct mol_atom_group *ag;
	struct agsetup *ags;
	struct mol_index_list lig_list;
	struct detailed_energy energy;
	struct restraints_params restraints;
	struct density_params density;
	bool enable_torsions;
	bool enable_electrostatics;
	bool enable_vdw;
	bool enable_density;
};


static void _zero_energies(struct detailed_energy *e)
{
	e->vdw = 0;
	e->vdw_s03 = 0;
	e->coulomb = 0;
	e->coulomb_s03 = 0;
	e->bonds = 0;
	e->angles = 0;
	e->impropers = 0;
	e->torsions = 0;
	e->restraints = 0;
	e->density = 0;
	e->genborn = 0;
}

static double _total_energy(const struct detailed_energy *e)
{
	return e->vdw + e->vdw_s03 + \
		e->coulomb + e->coulomb_s03 + \
		e->bonds + e->angles + e->impropers + e->torsions + \
		e->restraints + e->density + e->genborn;
}

static void _write_energies(
		const struct detailed_energy *e,
		const char *ofile)
{
	FILE *fout = fopen(ofile, "w");
	_die_if(fout == NULL,
		"Unable to open %s for writing", ofile);

	// It's a handmade JSON. I feel shame.
	fprintf(fout, "{");
#define _PRINT_TERM(name) do { \
	if (isfinite(e->name)) \
		fprintf(fout, "\"%s\": %.5f, ", #name, e->name); \
	else \
		fprintf(fout, "\"%s\": null, ", #name); \
	} while(0)
	_PRINT_TERM(vdw);
	_PRINT_TERM(vdw_s03);
	_PRINT_TERM(coulomb);
	_PRINT_TERM(coulomb_s03);
	_PRINT_TERM(bonds);
	_PRINT_TERM(angles);
	_PRINT_TERM(impropers);
	_PRINT_TERM(torsions);
	_PRINT_TERM(restraints);
	_PRINT_TERM(density);
	_PRINT_TERM(genborn);
#undef _PRINT_TERM
	double total = _total_energy(e);
	fprintf(fout, "\"total\": %.5f", total);
	fprintf(fout, "}\n");
	fclose(fout);
}


static void _set_beta_factors_for_output(
		struct mol_atom_group *ag,
		const struct mol_index_list list_rec,
		const struct mol_index_list list_lig,
		const struct mol_index_list list_restraints)
{
	// Mark beta: 0 - frozen atom; 1 - free receptor atom (at least on the last stage); 2 - free ligand atom; 3 - restrained ligand atom.
	for (size_t atom = 0; atom < ag->natoms; atom++)
		ag->B[atom] = 0;
	for (size_t i = 0; i < list_rec.size; i++) {
		size_t atom = list_rec.members[i];
		if (!ag->fixed[atom])
			ag->B[atom] = 1;
	}
	for (size_t i = 0; i < list_lig.size; i++) {
		size_t atom = list_lig.members[i];
		ag->B[atom] = 2;
	}
	for (size_t i = 0; i < list_restraints.size; i++) {
		size_t atom = list_restraints.members[i];
		ag->B[atom] = 3;
	}
}


// The \p atom_list contains the atoms in the atom group whose coordinates will be copied into \p array.
static void _coords_to_array_atom_list(
	struct mol_vector3 *dst,
	const struct mol_atom_group *ag,
	const struct mol_index_list atom_list)
{
	for (size_t j = 0; j < atom_list.size; j++) {
		size_t i = atom_list.members[j];
		MOL_VEC_COPY(dst[j], ag->coords[i]);
	}
}


static void _find_common_atoms_by_name(
		const struct mol_atom_group *ag_1,
		const struct mol_atom_group *ag_2,
		const struct mol_index_list list_to_use_in_ag_2,
		struct mol_index_list *list_common_1,
		struct mol_index_list *list_common_2)
{
#define MAX_ATOM_NAME_LEN 20  // Chosen semi-arbitrary
	size_t count = 0;
	size_t max_atoms = MAX(ag_1->natoms, ag_2->natoms);
	list_common_1->members = calloc(max_atoms, sizeof(size_t));
	list_common_2->members = calloc(max_atoms, sizeof(size_t));
	char name_i[MAX_ATOM_NAME_LEN], name_j[MAX_ATOM_NAME_LEN];

	for (size_t atom_i = 0; atom_i < ag_1->natoms; atom_i++) {
		smp_str_trim_and_copy(name_i, ag_1->atom_name[atom_i], MAX_ATOM_NAME_LEN);
		for (size_t j = 0; j < list_to_use_in_ag_2.size; j++) {
			size_t atom_j = list_to_use_in_ag_2.members[j];
			smp_str_trim_and_copy(name_j, ag_2->atom_name[atom_j], MAX_ATOM_NAME_LEN);
			if (strcmp(name_i, name_j) == 0) {
				list_common_1->members[count] = atom_i;
				list_common_2->members[count] = atom_j;
				count++;
				break;
			}
		}
	}

	list_common_1->members = realloc(list_common_1->members, count * sizeof(size_t));
	list_common_2->members = realloc(list_common_2->members, count * sizeof(size_t));
	list_common_1->size = count;
	list_common_2->size = count;
#undef MAX_ATOM_NAME_LEN
}


double potential(struct potential_params *pot_prms)
{
	static const double scale_vdw_s03 = 1.0;
	static const double scale_coul_s03 = 0.8333333;
	static const double eeps = 80.0;

	struct detailed_energy *energies = &(pot_prms->energy);

	check_clusterupdate(pot_prms->ag, pot_prms->ags);

	_zero_energies(energies);
	mol_zero_gradients(pot_prms->ag);

	if (pot_prms->enable_vdw) {
		vdweng(pot_prms->ag, &(energies->vdw), pot_prms->ags->nblst);
		vdwengs03(scale_vdw_s03, pot_prms->ags->nblst->nbcof, pot_prms->ag, &(energies->vdw_s03),
		          pot_prms->ags->nf03, pot_prms->ags->listf03);  // Most people call it 1-4. We call it 0-3
	}

	if (pot_prms->enable_electrostatics) {
		eleng(pot_prms->ag, eeps, &(energies->coulomb), pot_prms->ags->nblst);
		elengs03(scale_coul_s03, pot_prms->ags->nblst->nbcof, pot_prms->ag, eeps,
			&(energies->coulomb_s03), pot_prms->ags->nf03, pot_prms->ags->listf03);
	}
//    bool init = mol_init_gb(pot_prms->ag);
//    energies->genborn = mol_gb_energy(pot_prms->ag, 25.0, pot_prms->ags, MOL_GENBORN_OBC_2);
//    printf("%lf\n", energies->genborn);
//    bool del = mol_gb_delete_metadata(pot_prms->ag);
//    printf("%d\n", del);
	beng(pot_prms->ag, &(energies->bonds));
	aeng(pot_prms->ag, &(energies->angles));
	ieng(pot_prms->ag, &(energies->impropers));
	if (pot_prms->enable_torsions)
		teng(pot_prms->ag, &(energies->torsions));

	if (pot_prms->enable_density) {
		const struct mol_fitting_params den_prms = {
			.radius = pot_prms->density.radius,
			.mask = pot_prms->density.mask,
			.normalize = true};

		struct mol_vector3 *grads_backup = pot_prms->ag->gradients;
		if (pot_prms->density.no_grad) {
			pot_prms->ag->gradients = NULL;  // Prevent density from touching gradients
		}
		energies->density = mol_fitting_score(pot_prms->ag,
		                                      pot_prms->density.ref_ag,
		                                      &den_prms,
		                                      pot_prms->density.weight);
		pot_prms->ag->gradients = grads_backup;
	}

	smp_energy_restraints_fixedpoint(pot_prms->ag, &(energies->restraints),
		pot_prms->restraints.atom_indices,
		pot_prms->restraints.coords,
		pot_prms->restraints.k_spring);

	if (pot_prms->density.no_grad)
		return _total_energy(energies) - energies->density;
	else{
//	    printf("total eng: %lf\n", _total_energy(energies));
	    return _total_energy(energies);
	}
}

// Passed as a function pointer to the LBFGS
lbfgsfloatval_t potential_wrapper(
		void *prms,
		const double *inp,
		double *grad,
		const int n,
		__attribute__((unused)) const double step)
{
	struct potential_params *pot_prms = (struct potential_params*) prms;
	struct mol_atom_group *ag = pot_prms->ag;

	if (inp != NULL) {
		_die_if(n != ((int) ag->active_atoms->size) * 3,
			"Mismatch in in vector length");
		mol_atom_group_set_actives(ag, inp);
	}

	double energy = potential(pot_prms);

	if (grad != NULL) {
		for (int i = 0; i < n / 3; i++) {
			struct mol_vector3 neg_grad;
			MOL_VEC_MULT_SCALAR(neg_grad, ag->gradients[ag->active_atoms->members[i]], -1);
			SMP_VEC_TO_ARRAY(grad, i, neg_grad);
		}
	}
	return energy;
}


static void run_all_atom_min_simple(
		struct potential_params *pot_prms,
		const double cutoff,
		const double restraint_spring,
		const struct mol_index_list receptor_hydrogens)
{
	static const double min_tol = 1e-6;
	struct mol_atom_group *ag = pot_prms->ag;
	struct agsetup *ags = pot_prms->ags;
	struct mol_index_list lig_list = pot_prms->lig_list;

	// Unfreeze receptor hydrogens near ligand
	_freeze_all_unfreeze_atom_list(ag, lig_list);
	_unfreeze_atoms_from_list_if_near(ag, receptor_hydrogens, cutoff, lig_list);
	mol_fixed_update_active_lists(ag);
	update_nblst(ag, ags);

	pot_prms->enable_electrostatics = true;
	pot_prms->enable_vdw = true;
	pot_prms->enable_torsions = true;
	pot_prms->enable_density = false;

	pot_prms->restraints.k_spring = 0;
	mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

	pot_prms->restraints.k_spring = restraint_spring;
	mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

	pot_prms->restraints.k_spring = 0;
	pot_prms->enable_density = true;
	mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

	pot_prms->enable_density = false;
	mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

	// Run energy calculation once to get density score
	pot_prms->enable_density = true;
	pot_prms->density.no_grad = true;
	potential(pot_prms);
}



static void run_all_atom_min_multistage(
		struct potential_params *pot_prms,
		const double cutoff,
		const double restraint_spring,
		const struct mol_index_list ligand_all,
		const struct mol_index_list receptor_all,
		const int protocol)
{
	static const double min_tol = 1E-3;
	struct mol_atom_group *ag = pot_prms->ag;
	struct agsetup *ags = pot_prms->ags;

	_freeze_all_unfreeze_atom_list(ag, ligand_all);
	_unfreeze_atoms_from_list_if_near(ag, receptor_all, cutoff, ligand_all);
	mol_fixed_update_active_lists(ag);
	update_nblst(ag, ags);
	fix_list03(ag, ags->n03, ags->list03, &ags->nf03, ags->listf03);

	pot_prms->enable_torsions = true;
	pot_prms->enable_electrostatics = true;
	pot_prms->enable_vdw = true;
	pot_prms->enable_density = false;

	pot_prms->restraints.k_spring = restraint_spring;
	struct restraints_params restraints_lig_pulling;
	_duplicate_restraints_params(&restraints_lig_pulling, &pot_prms->restraints);

	// Create restraint params for all heavy atoms, properly pulling chosen ligand atoms and keeping everything else in place
	struct mol_index_list list_complex_heavy_not_pulled;
	_get_heavy(&list_complex_heavy_not_pulled, ag);
	_remove_from_list(&list_complex_heavy_not_pulled, &restraints_lig_pulling.atom_indices);
	struct restraints_params restraints_complex_heavy;
	_duplicate_restraints_params(&restraints_complex_heavy, &restraints_lig_pulling);
	_update_restraints_params_from_list(&restraints_complex_heavy, &list_complex_heavy_not_pulled, ag, true);


	struct mol_index_list ligand_not_pulled;
	_duplicate_list(&ligand_not_pulled, &ligand_all);
	_remove_from_list(&ligand_not_pulled, &restraints_lig_pulling.atom_indices);
	struct restraints_params restraints_ligand;
	_duplicate_restraints_params(&restraints_ligand, &restraints_lig_pulling);
	_update_restraints_params_from_list(&restraints_ligand, &ligand_not_pulled, ag, true);

	if (protocol == 3) {
		pot_prms->restraints = restraints_complex_heavy;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->restraints = restraints_ligand;
		mol_minimize_ag(MOL_LBFGS, 1500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->restraints.k_spring = restraint_spring / 1000;
		mol_minimize_ag(MOL_LBFGS, 1500, min_tol, ag, (void *) pot_prms, potential_wrapper);
	} else if (protocol == 2) {
		pot_prms->restraints = restraints_complex_heavy;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->restraints = restraints_ligand;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->restraints = restraints_lig_pulling;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->restraints.k_spring = 0;
		pot_prms->enable_density = true;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);

		pot_prms->enable_density = false;
		mol_minimize_ag(MOL_LBFGS, 500, min_tol, ag, (void *) pot_prms, potential_wrapper);
	} else {
		_die_if(true, "Unknown protocol number %d", protocol);
	}
	
	// Run energy calculation once to get density score
	pot_prms->enable_density = true;
	pot_prms->density.no_grad = true;
	double eng = potential(pot_prms);
	printf("%lf\n", eng);
}


void print_usage(const char *self_name)
{
	fprintf(stderr, "RMIN version %s\n\n", _GIT_VERSION_);
	fprintf(stderr, "USAGE: %s " \
		 "<complex.pdb> <complex.psf> " \
		 "<lig_resname> <lig_ref.pdb> " \
		 "<forcefield.prm> <topology.rtf> " \
		 "<out_min.pdb> <out_min.dat> [options]\n", self_name);
	fprintf(stderr, "   complex.pdb       - Complex PDB file.\n");
	fprintf(stderr, "   complex.psf       - Complex PSF file.\n");
	fprintf(stderr, "   lig_resname       - Residue name of ligand in complex.\n");
	fprintf(stderr, "   lig_ref.pdb       - Ligand reference PDB file (for RMSD and geometric restraints).\n");
	fprintf(stderr, "   forcefield.prm    - CHARMM-style forcefield parameters.\n");
	fprintf(stderr, "   topology.rtf      - CHARMM-style topology file.\n");
	fprintf(stderr, "   out_min.pdb       - PDB file to which lowest-energy structure should be written.\n");
	fprintf(stderr, "   out_min.dat       - File to which lowest-energy values should be written.\n");
	fprintf(stderr, " Options:\n");
	fprintf(stderr, "  -s  --spring=<n>    - Scale default restraints spring constant. Default: 1.\n");
	fprintf(stderr, "  -d  --density=<n>  - Scale default density constant. Default: 1.\n");
	fprintf(stderr, "  -c  --cutoff=<n>   - Receptor cutoff. Distance from the ligand center for which the receptor atoms are unfrozen. Default: 10 A.\n");
	fprintf(stderr, "  -p  --protocol=<n> - Minimization protocol. 1 - Simple, 2 - Flexible receptor, 3 - Flexible receptor, no relaxation. Default: 1\n");
	fprintf(stderr, "  -h  --help         - Print this message and quit.\n");
}


int main(int argc, char **argv)
{
	if (argc < 9) {
		print_usage(argv[0]);
		exit(EXIT_FAILURE);
	}
	// Default values
	double cutoff_around_ligand = 10.0; // Angstrom
	double restraints_k_spring = 1.0;
	double density_weight = 1.0;
	double density_radius = 2.0;
	int protocol = 1;

	// Buffers
	const char *cplx_pdb_file = strdup(argv[1]);
	const char *cplx_psf_file = strdup(argv[2]);
	const char *lig_resname = strdup(argv[3]);
	const char *pdb_file_restraints = strdup(argv[4]);
	const char *ff_prm_file = strdup(argv[5]);
	const char *ff_rtf_file = strdup(argv[6]);
	const char *out_pdb_file = strdup(argv[7]);
	const char *out_dat_file = strdup(argv[8]);

	static const struct option long_options[] = {
		{"spring", required_argument, NULL, 's'},
		{"density", required_argument, NULL, 'd'},
		{"cutoff", required_argument, NULL, 'c'},
		{"protocol", required_argument, NULL, 'p'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};

	while(1) {
		int option_index;
		int c = getopt_long(argc, argv, "d:s:c:p:h", long_options, &option_index);
		if (c == -1)
			break;
		switch(c) {
			case 'h':
				print_usage(argv[0]);
				exit(EXIT_SUCCESS);
			case 's':
				restraints_k_spring = atof(optarg);
				break;
			case 'd':
				density_weight = atof(optarg);
				break;
			case 'c':
				cutoff_around_ligand = atof(optarg);
				break;
			case 'p':
				protocol = atoi(optarg);
				break;
		}
	}

	printf("Core parameters:\n");
	printf(" Complex PDB: %s\n         PSF: %s\n", cplx_pdb_file, cplx_psf_file);
	printf(" PDB restraints: %s\n", pdb_file_restraints);
	printf(" Ligand residue name: %s\n", lig_resname);
	printf(" Forcefield parameters: %s\n            geometry: %s\n", ff_prm_file, ff_rtf_file);
	printf(" Output final structure PDB: %s\n        final energy DAT: %s\n", out_pdb_file, out_dat_file);
	printf(" Cutoff around ligand: %f A\n Restraints spring: %f\n", cutoff_around_ligand, restraints_k_spring);
	printf(" Density weight: %f\n Protocol number: %d\n", density_weight, protocol);

	// Load receptor
	struct mol_atom_group *ag = mol_read_pdb(cplx_pdb_file);
	_die_if(ag == NULL, "Unable to load complex PDB");
	bool read_ok = mol_atom_group_read_geometry(ag, cplx_psf_file, ff_prm_file, ff_rtf_file);
	_die_if(!read_ok, "Unable to load geometry (psf and forcefield) for the complex");

	// Set up list of receptor atoms in complex that will be created later
	struct mol_index_list list_rec, list_lig;
	list_rec.size = 0;
	list_lig.size = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (strcmp(ag->residue_name[i], lig_resname) == 0)
			list_lig.size++;
		else
			list_rec.size++;
	}
	list_rec.members = calloc(list_rec.size, sizeof(size_t));
	list_lig.members = calloc(list_lig.size, sizeof(size_t));
	size_t lig_counter = 0, rec_counter = 0;
	for (size_t i = 0; i < ag->natoms; i++) {
		if (strcmp(ag->residue_name[i], lig_resname) == 0)
			list_lig.members[lig_counter++] = i;
		else
			list_rec.members[rec_counter++] = i;
	}

	// Load restraints
	struct mol_atom_group *ag_reference = mol_read_pdb(pdb_file_restraints);
	_die_if(ag_reference == NULL, "Unable to load the restraint file");

	mol_fixed_init(ag);

	// Setup non-bonded list
	struct agsetup ags;
	init_nblst(ag, &ags);
	update_nblst(ag, &ags);

	// For all-atom minimization find hydrogens receptors
	struct mol_index_list list_hydrogens;
	smp_get_atom_list_by_element(&list_hydrogens, ag, "H", true);


	// Find atoms in ligand for which reference constraints
	struct mol_index_list list_restrained_in_ag;
	struct mol_index_list list_restrained_in_ref;
	_find_common_atoms_by_name(ag_reference, ag, list_lig, &list_restrained_in_ref, &list_restrained_in_ag);
	_die_if(list_restrained_in_ag.size == 0,
		"No common atoms between ligand and restraints references!");
	struct mol_vector3 *restraints_coords = calloc(list_restrained_in_ref.size, sizeof(struct mol_vector3));
	_coords_to_array_atom_list(restraints_coords, ag_reference, list_restrained_in_ref);

	// Initialize structure with energy calculation parameters
	// Set up pointers to current ag, ags, and all other needed data.
	struct potential_params pot_prms;
	pot_prms.ag = ag;
	pot_prms.ags = &ags;
	pot_prms.lig_list = list_lig;
	pot_prms.restraints.k_spring = restraints_k_spring;
	pot_prms.restraints.atom_indices = list_restrained_in_ag;
	pot_prms.restraints.coords = restraints_coords;
	pot_prms.density.ref_ag = ag_reference;
	pot_prms.density.weight = density_weight;
	pot_prms.density.radius = density_radius;
	_zero_energies(&pot_prms.energy);

	// Setup density params
	bool *density_mask = calloc(ag->natoms, sizeof(bool)); // false by default
	for (size_t i = 0; i < list_lig.size; i++) {
		density_mask[list_lig.members[i]] = true;
	}
	pot_prms.density.mask = density_mask;
	pot_prms.density.no_grad = false;

	// All-atom minimization
	switch (protocol) {
		case 1:
			run_all_atom_min_simple((void *) (&pot_prms), cutoff_around_ligand, restraints_k_spring, list_hydrogens);
			break;
		case 2: // fall thru
		case 3:
			run_all_atom_min_multistage((void *) (&pot_prms), cutoff_around_ligand, restraints_k_spring,
				list_lig, list_rec, protocol);
//			struct detailed_energy *energies = &(pot_prms.energy);
//	        printf("%lf\n", energies->genborn, energies->bonds);
			break;
		default:
			fprintf(stderr, "[error] Wrong protocol number\n");
			exit(EXIT_FAILURE);
	}

	// Save low energy structure
	_set_beta_factors_for_output(ag, list_rec, list_lig, list_restrained_in_ag);
	mol_write_pdb(out_pdb_file, ag);
//	struct detailed_energy *energies = &(pot_prms.energy);
//	printf("%lf %lf\n", energies->genborn, energies->bonds);
	_write_energies(&(pot_prms.energy), out_dat_file);

	free(density_mask);

	return 0;
}
