from django.contrib import admin

from .models import Job, ModellerKey

admin.site.register(Job)
admin.site.register(ModellerKey)
